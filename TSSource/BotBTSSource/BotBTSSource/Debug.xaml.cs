﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Security.Principal;
using System.Threading;
using System.Runtime.CompilerServices;

namespace BotBTSSource
{
    /// <summary>
    /// Interaction logic for Debug.xaml
    /// </summary>
    public partial class Debug : Window
    {
        public static string cpuName = Environment.UserName;
        public static string debugFolder = @"C:\Users\" + cpuName + @"\Documents\BotB Player Files\Debug\";
        public bool isTournamentOver = false;
        DirectoryInfo dbDir = new DirectoryInfo(debugFolder);

        public Debug()
        {
            InitializeComponent();
        }

        public void buildListBox()
        {
            foreach (var file in dbDir.GetFiles())
            {
                if (file.Name == ((Tournament)this.Owner).dbgName)
                {
                    string[] allLines = File.ReadAllLines(file.FullName);
                    for (int i = 0; i < allLines.Length; i++)
                    {
                        debugMsg_listBox.Items.Add(allLines[i]);
                    }
                }
            }
        }

        public void writeDebugMessage(string message, string className, [CallerLineNumber] int lineNumber = 0)
        {
            string lineToWrite = DateTime.Now.ToString("HH:mm:ss ") + message + " @ " + className + " Line " + lineNumber;
            File.AppendAllText(((Tournament)this.Owner).dbgFilePath, Environment.NewLine + lineToWrite);
            debugMsg_listBox.Items.Add(lineToWrite);
            debugMsg_listBox.Items.Refresh();
            UpdateScrollBar(debugMsg_listBox);
        }

        private void DebugWindow_Closing(object sender, CancelEventArgs e)
        {
            if(isTournamentOver)
            {
                MainWindow mainWin = new MainWindow();
                mainWin.Show();
                ((Tournament)this.Owner).Close();
            }
            else
            {
                ((Tournament)this.Owner).debugOn = false;
                ((Tournament)this.Owner).debug_checkBox.IsChecked = false;
            }
        }

        private void UpdateScrollBar(ListBox listBox)
        {
            if (listBox != null)
            {
                var border = (Border)VisualTreeHelper.GetChild(listBox, 0);
                var scrollViewer = (ScrollViewer)VisualTreeHelper.GetChild(border, 0);
                scrollViewer.ScrollToBottom();
            }
        }
    }
}
