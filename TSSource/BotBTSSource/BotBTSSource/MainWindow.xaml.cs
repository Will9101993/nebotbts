﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Security.Principal;

namespace BotBTSSource
{
    public partial class MainWindow : Window
    {

        public static string cpuName = Environment.UserName;
        public static string playerFolder = @"C:\Users\" + cpuName + @"\Documents\BotB Player Files\";
        DirectoryInfo dir = new DirectoryInfo(playerFolder);
        public BindingList<string> players = new BindingList<string>();
        string playerToRemove;

        public MainWindow()
        {
            InitializeComponent();
            if (Directory.Exists(playerFolder))
            {
                //Do Nothing
            }
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(playerFolder);
            }
            playerList.ItemsSource = players;
        }

        private void info_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<PlayerManager>().FirstOrDefault();
            if (doesWinExist == null)
            {
                Info infoWin = new Info();
                infoWin.Show();
            }
            else
            {
                doesWinExist.Activate();
            }
        }

        private void playerManager_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<PlayerManager>().FirstOrDefault();
            if (doesWinExist == null)
            {
                PlayerManager playerManagerWin = new PlayerManager();
                playerManagerWin.Owner = this;
                playerManagerWin.Show();
                playerManagerWin.parentName = "Main Window";
            }
            else
            {
                doesWinExist.Activate();
            }
        }

        private void addButton_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<AddPlayers>().FirstOrDefault();
            if (doesWinExist == null)
            {
                AddPlayers addPlayersWin = new AddPlayers();
                addPlayersWin.Owner = this;
                addPlayersWin.Show();
                addPlayersWin.loadPlayers();
            }
            else
            {
                doesWinExist.Activate();
            }
        }

        private void removeButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                playerToRemove = playerList.SelectedItem.ToString();
                for (int i = 0; i < players.Count; i++)
                {
                    if (players[i].Equals(playerToRemove))
                    {
                        players.RemoveAt(i);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Exception Caught\n" + "Make sure to select a valid player\n" + "Before clicking remove", "Exception");
            }
        }

        private void startButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (players.Count() > 3)
            {
                try
                {
                    var doesWinExist = Application.Current.Windows.OfType<TournamentConfirm>().FirstOrDefault();
                    if (doesWinExist == null)
                    {
                        TournamentConfirm tournConfirmWin = new TournamentConfirm();
                        tournConfirmWin.Owner = this;
                        tournConfirmWin.playerCount_textBlock.Text = players.Count.ToString();
                        tournConfirmWin.ShowDialog();
                    }
                    else
                    {
                        doesWinExist.Activate();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Exception Caught\n" + "How did this happen?\n" + "Contact Will with what you dun goofed on", "Exception");
                }
            }
            else
            {
                MessageBox.Show("Exception Caught\n" + "Add four or more players\n" + "Before starting the tournament", "Exception");
            }
        }

        public void updateEnrollList()
        {
            playerList.Items.Refresh();
        }

        public void enrollListNameUpdate(string id, string name)
        {
            string properName = id + " | " + name;
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].Contains(id))
                {
                    players[i] = properName;
                    break;
                }
                else
                {
                    //Do Nothing
                }
            }
        }

        public void cleanList(string id, string name)
        {
            string properName = id + " | " + name;
            if (players.Contains(properName))
            {
                players.Remove(properName);
            }
        }

        private void pegasusEvents_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<PegasusFormat>().FirstOrDefault();
            if (doesWinExist == null)
            {
                PegasusFormat pegWin = new PegasusFormat();
                pegWin.Show();
                pegWin.loadRules();
            }
            else
            {
                doesWinExist.Activate();
            }
        }
    }
}
