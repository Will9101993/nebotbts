﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Security.Principal;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Drawing;


namespace BotBTSSource
{
    public partial class Tournament : Window
    {
        System.Windows.Threading.DispatcherTimer timer1 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer timer2 = new System.Windows.Threading.DispatcherTimer();
        public BindingList<string> players = new BindingList<string>();
        public BindingList<string> pairedPlayers = new BindingList<string>();
        public BindingList<string> roundInfo = new BindingList<string>();
        public List<string> tournamentInfo = new List<string>();
        public List<string> playersByTb = new List<string>();
        public List<string> playersByTbSS = new List<string>();
        public List<string> playersToPD = new List<string>();
        public List<string> pastPairings = new List<string>();
        public List<string> playersToDrop = new List<string>();
        public List<string> playersDropped = new List<string>();
        public List<string> topCutPlayers = new List<string>();
        public List<string> topCut = new List<string>();
        public List<string> pegasusRules = new List<string>();
        public List<string> usedRules = new List<string>();
        public Debug debugRef;
        public static string path = MainWindow.playerFolder + @"\Tournament Files\";
        public static string dbPath = MainWindow.playerFolder;
        DirectoryInfo dir = new DirectoryInfo(path);
        public DirectoryInfo dbDir = new DirectoryInfo(dbPath);
        public int round = 0;
        public int minutes;
        public int pMinutes;
        int seconds;
        int pSeconds;
        int table;
        int p1Checked;
        int p2Checked;
        int tieBreaker;
        int gameWins;
        int gameLosses;
        int matchWins;
        int matchLosses;
        int numPairingsCreated;
        int totalGames;
        int totalMatches;
        int mwpInt;
        int gwpInt;
        int numCut;
        public int timeEntered;
        public int pTimeEntered;
        float mwp;
        float gwp;
        string minutesStr;
        string secondsStr;
        string pMinutesStr;
        string pSecondsStr;
        string filePath;
        public string lastEvent = "Nothing yet!";
        public string dbgName;
        public string dbgFilePath;
        public string invalidPlayer;
        public string tournamentName;
        public string tournamentType;
        public bool wasPlayerRemoved = false;
        public bool bo1;
        public bool hasTimerRan = false;
        public bool pHasTimerRan = false;
        public bool warned = false;
        public bool reWarning = false;
        public bool invalidPairing = false;
        public bool dropBye = false;
        public bool debugOn = false;
        public bool split = false;
        public bool cutting = false;
        public bool dbgFileCreated = false;
        public bool pegasusFormat = false;
        public bool excludeJudgeOrg = false;

        public Tournament()
        {
            InitializeComponent();
            if (Directory.Exists(path))
            {
                foreach (var file in dir.GetFiles())
                {
                    file.Delete();
                }
            }
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(path);
            }
        }

        public void initialSetup()
        {
            if (timeEntered > -1)
            {
                timer_textBlock.Text = timeEntered.ToString() + ":" + "00";
            }
            else
            {
                //Do Nothing
            }

            if(pegasusFormat == true)
            {
                pFormTimer_textBlock.Text = pTimeEntered.ToString() + ":" + "00";
            }
            else
            {
                //Do Nothing
            }

            /*string[] tournamentStruct = { "Tournament Name:", tournamentName , "Players:" , };
            tournamentInfo.Add("Tournament Name:");
            tournamentInfo.Add(tournamentName);
            tournamentInfo.Add("Players:");*/
            if (players.Count() % 2 == 0)
            {

            }
            else
            {
                players.Add("000000 | BYE");
            }

            for (int i = 0; i < players.Count(); i++)
            {
                tournamentInfo.Add(players[i] /*Name*/);
                tournamentInfo.Add("0" /*Tiebreaker*/);
                tournamentInfo.Add("0" /*Game Wins*/);
                tournamentInfo.Add("0" /*Game Losses*/);
                tournamentInfo.Add("0" /*Match Wins*/);
                tournamentInfo.Add("0" /*Match Losses*/);
            }

            filePath = path + tournamentName + ".txt";
            File.WriteAllLines(filePath, tournamentInfo);
        }

        public void startTimer()
        {
            if (timeEntered > -1)
            {
                minutes = timeEntered;
                seconds = 0;
                if (timer1.IsEnabled)
                {
                    timer1.Stop();
                }

                if (!hasTimerRan)
                {
                    timer1.Tick += timer1_Tick;
                    timer1.Interval = new TimeSpan(0, 0, 1);
                }

                timer1.Start();
                hasTimerRan = true;
            }
            else
            {
                //Do Nothing
            }
        }

        public void pStartTimer()
        {
            if (pTimeEntered > -1)
            {
                pMinutes = pTimeEntered;
                pSeconds = 0;
                if (timer2.IsEnabled)
                {
                    timer2.Stop();
                }

                if (!pHasTimerRan)
                {
                    timer2.Tick += timer2_Tick;
                    timer2.Interval = new TimeSpan(0, 0, 1);
                }

                timer2.Start();
                pHasTimerRan = true;
            }
            else
            {
                //Do Nothing
            }
        }

        public void startTimerExt()
        {
            if (timeEntered > -1)
            {
                if (debugOn)
                {
                    debugRef.writeDebugMessage("Starting timer with a " + minutes + " minute extension.", this.GetType().Name);
                }
                seconds = 0;
                if (timer1.IsEnabled)
                {
                    timer1.Stop();
                }

                if (!hasTimerRan)
                {
                    timer1.Tick += timer1_Tick;
                    timer1.Interval = new TimeSpan(0, 0, 1);
                }

                timer1.Start();
                hasTimerRan = true;
            }
            else
            {
                //Do Nothing
            }
        }

        private void pairButton_OnClick(object sender, RoutedEventArgs e)
        {
            bool resultsIn = true;
            Random rand = new Random();
            table = 1;

            round++;

            if (round == 1)
            {
                if (timeEntered > -1)
                {
                    startTimer();
                    pausePlay_Button.Content = "⏸️";
                }
                else
                {
                    //Do Nothing
                }

                if (pegasusFormat == true)
                {
                    lastEvent = "Nothing yet!";
                    if (usedRules.Count > 0)
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("Leftovers in usedRules, resetting...", this.GetType().Name);
                        }

                        var rulesToReset = usedRules.Where(item => item.Length > 0).ToList();
                        usedRules = usedRules.Except(rulesToReset).ToList();
                        pegasusRules.AddRange(rulesToReset);

                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("All usedRules should be returned to pegasusRules.", this.GetType().Name);
                        }

                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("usedRules new count: " + usedRules.Count, this.GetType().Name);
                        }

                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("pegasusRules new count: " + pegasusRules.Count, this.GetType().Name);
                        }
                    }
                    pStartTimer();
                    pausePlay_Button_Pegasus.Content = "⏸️";
                }
                else
                {
                    //Do Nothing
                }
                if (debugOn)
                {
                    debugRef.writeDebugMessage("Initializing Tournament...", this.GetType().Name);
                }

                for (int i = 0; i < players.Count(); i++)
                {
                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Pairing Round 1...", this.GetType().Name);
                    }
                    validatePlayer(pairedPlayers);
                }

                for (int i = 0; i < pairedPlayers.Count(); i++)
                {
                    if (i % 2 == 0)
                    {
                        //Do Nothing
                    }
                    else
                    {
                        if (pairedPlayers[i - 1].Contains("000000 | BYE") || pairedPlayers[i].Contains("000000 | BYE"))
                        {
                            if (!pairedPlayers[i - 1].Contains("000000 | BYE"))
                            {
                                byeList.Items.Add(pairedPlayers[i - 1]);
                            }
                            else
                            {
                                byeList.Items.Add(pairedPlayers[i]);
                            }
                        }
                        else
                        {
                            if (debugOn)
                            {
                                debugRef.writeDebugMessage("Creating pairing: " + pairedPlayers[i] + " & " + pairedPlayers[i - 1], this.GetType().Name);
                            }

                            pairingList.Items.Add(new PlayerPairingHandler
                            {
                                playerTwoName = pairedPlayers[i],
                                playerOneName = pairedPlayers[i - 1],
                                results = "0-0",
                                table = table.ToString(),
                                vs = "vs",
                            });
                        }
                        pastPairings.Add(pairedPlayers[i].Substring(0, 6) + pairedPlayers[i - 1].Substring(0, 6));
                        pastPairings.Add(pairedPlayers[i - 1].Substring(0, 6) + pairedPlayers[i].Substring(0, 6));
                        if (pairedPlayers[i - 1].Contains("000000 | BYE") || pairedPlayers[i].Contains("000000 | BYE"))
                        {
                            //Don't Increment Table
                        }
                        else
                        {
                            table++;
                        }
                    }
                }
                roundNum_textBlock.Text = "Round: " + round;
            }
            else
            {
                for (int i = 0; i < pairingList.Items.Count; i++)
                {
                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                    if (pph.results == "0-0" || pph.results == "1-0" || pph.results == "0-1")
                    {
                        resultsIn = false;
                    }
                }

                switch (resultsIn)
                {
                    case true:
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("Initializing Round " + round + "...", this.GetType().Name);
                        }
                        if (cutting)
                        {
                            int numResultsFound = 0;
                            for (int i = 0; i < pairingList.Items.Count; i++)
                            {
                                numResultsFound = 0;
                                PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                                roundInfo.Add("Table: ");
                                roundInfo.Add(pph.table);
                                roundInfo.Add(pph.playerOneName);
                                roundInfo.Add(pph.playerTwoName);
                                if (pph.g1 != "null" && !string.IsNullOrEmpty(pph.g1))
                                {
                                    roundInfo.Add("G1: " + pph.g1);
                                }
                                else
                                {
                                    roundInfo.Add("G1: NA");
                                }
                                if (pph.g2 != "null" && !string.IsNullOrEmpty(pph.g2))
                                {
                                    roundInfo.Add("G2: " + pph.g2);
                                }
                                else
                                {
                                    roundInfo.Add("G2: NA");
                                }
                                if (pph.g3 != "null" && !string.IsNullOrEmpty(pph.g3))
                                {
                                    roundInfo.Add("G3: " + pph.g3);
                                }
                                else
                                {
                                    roundInfo.Add("G3: NA");
                                }
                                roundInfo.Add("Results: ");
                                roundInfo.Add(pph.results);

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Setting results for: " + pph.playerOneName + " vs " + pph.playerTwoName, this.GetType().Name);
                                }

                                if (pph.results.Substring(0, 1) == "2")
                                {
                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            string[] allLines = File.ReadAllLines(file.FullName);
                                            for (int a = 0; a < allLines.Length; a++)
                                            {
                                                if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                {
                                                    if (debugOn)
                                                    {
                                                        debugRef.writeDebugMessage(pph.playerOneName + " won the match, updating results...", this.GetType().Name);
                                                    }

                                                    tieBreaker = Int32.Parse(allLines[a + 1]);
                                                    gameWins = Int32.Parse(allLines[a + 2]);
                                                    gameLosses = Int32.Parse(allLines[a + 3]);
                                                    matchWins = Int32.Parse(allLines[a + 4]);
                                                    matchLosses = Int32.Parse(allLines[a + 5]);

                                                    tieBreaker = tieBreaker + 1;
                                                    gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                    gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                    matchWins++;

                                                    allLines[a + 1] = tieBreaker.ToString();
                                                    allLines[a + 2] = gameWins.ToString();
                                                    allLines[a + 3] = gameLosses.ToString();
                                                    allLines[a + 4] = matchWins.ToString();
                                                    allLines[a + 5] = matchLosses.ToString();
                                                    numResultsFound++;
                                                }
                                                else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                {
                                                    if (debugOn)
                                                    {
                                                        debugRef.writeDebugMessage(pph.playerTwoName + " lost the match, updating results...", this.GetType().Name);
                                                    }

                                                    tieBreaker = Int32.Parse(allLines[a + 1]);
                                                    gameWins = Int32.Parse(allLines[a + 2]);
                                                    gameLosses = Int32.Parse(allLines[a + 3]);
                                                    matchWins = Int32.Parse(allLines[a + 4]);
                                                    matchLosses = Int32.Parse(allLines[a + 5]);

                                                    gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                    gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                    matchLosses++;

                                                    allLines[a + 1] = tieBreaker.ToString();
                                                    allLines[a + 2] = gameWins.ToString();
                                                    allLines[a + 3] = gameLosses.ToString();
                                                    allLines[a + 4] = matchWins.ToString();
                                                    allLines[a + 5] = matchLosses.ToString();
                                                    cleanList(topCut, pph.playerTwoName);
                                                    topCutPlayers.Add(pph.playerTwoName);
                                                    numResultsFound++;
                                                }
                                                else if (numResultsFound == 2)
                                                {
                                                    break;
                                                }
                                            }
                                            File.WriteAllLines(file.FullName, allLines);
                                        }
                                    }
                                }
                                else if (pph.results.Substring(2, 1) == "2")
                                {
                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            string[] allLines = File.ReadAllLines(file.FullName);
                                            for (int a = 0; a < allLines.Length; a++)
                                            {
                                                if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                {
                                                    if (debugOn)
                                                    {
                                                        debugRef.writeDebugMessage(pph.playerTwoName + " won the match, updating results...", this.GetType().Name);
                                                    }
                                                    tieBreaker = Int32.Parse(allLines[a + 1]);
                                                    gameWins = Int32.Parse(allLines[a + 2]);
                                                    gameLosses = Int32.Parse(allLines[a + 3]);
                                                    matchWins = Int32.Parse(allLines[a + 4]);
                                                    matchLosses = Int32.Parse(allLines[a + 5]);

                                                    tieBreaker = tieBreaker + 1;
                                                    gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                    gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                    matchWins++;

                                                    allLines[a + 1] = tieBreaker.ToString();
                                                    allLines[a + 2] = gameWins.ToString();
                                                    allLines[a + 3] = gameLosses.ToString();
                                                    allLines[a + 4] = matchWins.ToString();
                                                    allLines[a + 5] = matchLosses.ToString();
                                                    numResultsFound++;
                                                }
                                                else if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                {
                                                    if (debugOn)
                                                    {
                                                        debugRef.writeDebugMessage(pph.playerOneName + " lost the match, updating results...", this.GetType().Name);
                                                    }

                                                    tieBreaker = Int32.Parse(allLines[a + 1]);
                                                    gameWins = Int32.Parse(allLines[a + 2]);
                                                    gameLosses = Int32.Parse(allLines[a + 3]);
                                                    matchWins = Int32.Parse(allLines[a + 4]);
                                                    matchLosses = Int32.Parse(allLines[a + 5]);

                                                    gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                    gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                    matchLosses++;

                                                    allLines[a + 1] = tieBreaker.ToString();
                                                    allLines[a + 2] = gameWins.ToString();
                                                    allLines[a + 3] = gameLosses.ToString();
                                                    allLines[a + 4] = matchWins.ToString();
                                                    allLines[a + 5] = matchLosses.ToString();
                                                    cleanList(topCut, pph.playerOneName);
                                                    topCutPlayers.Add(pph.playerOneName);
                                                    numResultsFound++;
                                                }
                                                else if (numResultsFound == 2)
                                                {
                                                    break;
                                                }
                                            }
                                            File.WriteAllLines(file.FullName, allLines);
                                        }
                                    }
                                }
                                else
                                {
                                    //Uhhhh this isn't supposed to happen, something went wrong.
                                }
                            }

                            round--;
                            filePath = path + "(Cut)Round " + round + ".txt";
                            round++;
                            File.WriteAllLines(filePath, roundInfo);
                            clearInformation(pairedPlayers);

                            if(topCut.Count >= 2)
                            {
                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Pairing Cut Round " + round + "...", this.GetType().Name);
                                }
                                roundNum_textBlock.Text = "Round: " + round + " (Cut)";
                                for (int i = 0; i < topCut.Count; i++)
                                {

                                    if (i % 2 == 0 && i <= numCut - 1)
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Pairing: " + topCut[i + 1] + " & " + topCut[i], this.GetType().Name);
                                        }

                                        pairingList.Items.Add(new PlayerPairingHandler
                                        {
                                            playerTwoName = topCut[i + 1],
                                            playerOneName = topCut[i],
                                            results = "0-0",
                                            table = table.ToString(),
                                            vs = "vs",
                                        });
                                        table++;
                                    }
                                }
                            }
                            else
                            {
                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Tournament does not have enough players to continue, ending...", this.GetType().Name);
                                }

                                topCutPlayers.Add(topCut[0]);
                                endTournament(topCutPlayers);
                            }
                            //pair logic goes here
                        }
                        else
                        {
                            int numResultsFound = 0;
                            if (playersToDrop.Count > 0)
                            {
                                string dropPlayersDisp = "";
                                for (int i = 0; i < playersToDrop.Count; i++)
                                {
                                    dropPlayersDisp = dropPlayersDisp + "\n" + playersToDrop[i];
                                }
                                MessageBoxResult confirmResult = MessageBox.Show("You're about to drop\n" + dropPlayersDisp + "\n\nAre you sure?", "Confirmation Box", MessageBoxButton.YesNo);
                                if (confirmResult == MessageBoxResult.Yes)
                                {
                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Dropping players has been confirmed...", this.GetType().Name);
                                    }
                                    if (byeList.Items.Count > 0)
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Removing BYE...", this.GetType().Name);
                                        }
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int i = 0; i < allLines.Length; i++)
                                                {
                                                    if (allLines[i].Contains(byeList.Items[0].ToString()))
                                                    {
                                                        tieBreaker = Int32.Parse(allLines[i + 1]);
                                                        gameWins = Int32.Parse(allLines[i + 2]);
                                                        gameLosses = Int32.Parse(allLines[i + 3]);
                                                        matchWins = Int32.Parse(allLines[i + 4]);
                                                        matchLosses = Int32.Parse(allLines[i + 5]);

                                                        tieBreaker = tieBreaker + 1;

                                                        allLines[i + 1] = tieBreaker.ToString();
                                                        allLines[i + 2] = gameWins.ToString();
                                                        allLines[i + 3] = gameLosses.ToString();
                                                        allLines[i + 4] = matchWins.ToString();
                                                        allLines[i + 5] = matchLosses.ToString();
                                                    }
                                                }
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                    }
                                    for (int i = 0; i < pairingList.Items.Count; i++)
                                    {
                                        numResultsFound = 0;
                                        PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                                        roundInfo.Add("Table: ");
                                        roundInfo.Add(pph.table);
                                        roundInfo.Add(pph.playerOneName);
                                        roundInfo.Add(pph.playerTwoName);
                                        if (pph.g1 != "null" && !string.IsNullOrEmpty(pph.g1))
                                        {
                                            roundInfo.Add("G1: " + pph.g1);
                                        }
                                        else
                                        {
                                            roundInfo.Add("G1: NA");
                                        }
                                        if (pph.g2 != "null" && !string.IsNullOrEmpty(pph.g2))
                                        {
                                            roundInfo.Add("G2: " + pph.g2);
                                        }
                                        else
                                        {
                                            roundInfo.Add("G2: NA");
                                        }
                                        if (pph.g3 != "null" && !string.IsNullOrEmpty(pph.g3))
                                        {
                                            roundInfo.Add("G3: " + pph.g3);
                                        }
                                        else
                                        {
                                            roundInfo.Add("G3: NA");
                                        }
                                        roundInfo.Add("Results: ");
                                        roundInfo.Add(pph.results);

                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Setting results for: " + pph.playerOneName + " vs " + pph.playerTwoName, this.GetType().Name);
                                        }

                                        if (pph.results.Substring(0, 1) == "2")
                                        {
                                            foreach (var file in dir.GetFiles())
                                            {
                                                if (file.Name == tournamentName + ".txt")
                                                {
                                                    string[] allLines = File.ReadAllLines(file.FullName);
                                                    for (int a = 0; a < allLines.Length; a++)
                                                    {
                                                        if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                        {
                                                            if (debugOn)
                                                            {
                                                                debugRef.writeDebugMessage(pph.playerOneName + " won the match, updating results...", this.GetType().Name);
                                                            }

                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            tieBreaker = tieBreaker + 1;
                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                            matchWins++;

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                        {
                                                            if (debugOn)
                                                            {
                                                                debugRef.writeDebugMessage(pph.playerTwoName + " lost the match, updating results...", this.GetType().Name);
                                                            }

                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                            matchLosses++;

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (numResultsFound == 2)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    File.WriteAllLines(file.FullName, allLines);
                                                }
                                            }
                                        }
                                        else if (pph.results.Substring(2, 1) == "2")
                                        {
                                            foreach (var file in dir.GetFiles())
                                            {
                                                if (file.Name == tournamentName + ".txt")
                                                {
                                                    string[] allLines = File.ReadAllLines(file.FullName);
                                                    for (int a = 0; a < allLines.Length; a++)
                                                    {
                                                        if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                        {
                                                            if (debugOn)
                                                            {
                                                                debugRef.writeDebugMessage(pph.playerTwoName + " won the match, updating results...", this.GetType().Name);
                                                            }

                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            tieBreaker = tieBreaker + 1;
                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                            matchWins++;

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                        {
                                                            if (debugOn)
                                                            {
                                                                debugRef.writeDebugMessage(pph.playerOneName + " lost the match, updating results...", this.GetType().Name);
                                                            }

                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                            matchLosses++;

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (numResultsFound == 2)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    File.WriteAllLines(file.FullName, allLines);
                                                }
                                            }
                                        }
                                        else if (pph.results == "1-1")
                                        {
                                            if (debugOn)
                                            {
                                                debugRef.writeDebugMessage("Match was a draw, updating results...", this.GetType().Name);
                                            }

                                            foreach (var file in dir.GetFiles())
                                            {
                                                if (file.Name == tournamentName + ".txt")
                                                {
                                                    string[] allLines = File.ReadAllLines(file.FullName);
                                                    for (int a = 0; a < allLines.Length; a++)
                                                    {
                                                        if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                        {
                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                        {
                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (numResultsFound == 2)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    File.WriteAllLines(file.FullName, allLines);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Uhhhh this isn't supposed to happen, something went wrong.
                                        }
                                    }
                                    if (byeList.Items.Count > 0)
                                    {
                                        roundInfo.Add("BYE Player(s): ");
                                        roundInfo.Add(byeList.Items.GetItemAt(0).ToString());
                                    }

                                    if (!warned)
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Updating our past pairings list...", this.GetType().Name);
                                        }
                                        for (int i = 0; i < pairingList.Items.Count; i++)
                                        {
                                            PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                                            pastPairings.Add(pph.playerOneName.Substring(0, 6) + pph.playerTwoName.Substring(0, 6));
                                            pastPairings.Add(pph.playerTwoName.Substring(0, 6) + pph.playerOneName.Substring(0, 6));
                                        }
                                    }

                                    if (debugOn)
                                    {
                                        int roundRef = round - 1;
                                        debugRef.writeDebugMessage("Past pairings as of Round " + roundRef + ":", this.GetType().Name);
                                        for (int i = 0; i < pastPairings.Count; i++)
                                        {
                                            debugRef.writeDebugMessage(pastPairings[i].Substring(0,6) + " vs " + pastPairings[i].Substring(6,6), this.GetType().Name);
                                        }
                                    }


                                    round--;
                                    filePath = path + "Round " + round + ".txt";
                                    round++;

                                    File.WriteAllLines(filePath, roundInfo);

                                    if (playersToDrop.Count > 0)
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Dropping players...", this.GetType().Name);
                                        }

                                        for (int i = 0; i < playersToDrop.Count; i++)
                                        {
                                            playersDropped.Add(playersToDrop[i]);

                                            if (debugOn)
                                            {
                                                debugRef.writeDebugMessage(playersToDrop[i] + " has been added to dropped list.", this.GetType().Name);
                                            }
                                        }

                                        for (int i = 0; i < players.Count; i++)
                                        {
                                            for (int a = 0; a < playersToDrop.Count; a++)
                                            {
                                                if (players[i].Contains(playersToDrop[a].Substring(0, 6)))
                                                {
                                                    players.RemoveAt(i);

                                                    if (debugOn)
                                                    {
                                                        debugRef.writeDebugMessage(playersToDrop[a] + " was removed from player list.", this.GetType().Name);
                                                    }

                                                    if (i != 0)
                                                    {
                                                        i--;
                                                    }
                                                }

                                                if (players[i].Contains("000000 | BYE"))
                                                {
                                                    players.RemoveAt(i);

                                                    if (debugOn)
                                                    {
                                                        debugRef.writeDebugMessage("BYE has been removed.", this.GetType().Name);
                                                    }

                                                    if (i != 0)
                                                    {
                                                        i--;
                                                    }
                                                }
                                            }
                                        }

                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Clearing our playersToDrop list...", this.GetType().Name);
                                        }

                                        playersToDrop.Clear();
                                    }

                                    if (players.Count() % 2 == 0)
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                                for (int i = 0; i < allLines.Count; i++)
                                                {
                                                    if (allLines[i].Contains("000000 | BYE"))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage("Removing BYE...", this.GetType().Name);
                                                        }

                                                        for (int b = 0; b < 6; b++)
                                                        {
                                                            allLines.RemoveAt(i);
                                                        }
                                                        File.WriteAllLines(file.FullName, allLines);
                                                        allLines = File.ReadAllLines(file.FullName).ToList();
                                                        i = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                                for (int i = 0; i < allLines.Count; i++)
                                                {
                                                    if (allLines[i].Contains("000000 | BYE"))
                                                    {
                                                        for (int b = 0; b < 6; b++)
                                                        {
                                                            allLines.RemoveAt(i);
                                                        }

                                                        File.WriteAllLines(file.FullName, allLines);
                                                        allLines = File.ReadAllLines(file.FullName).ToList();
                                                        i = 0;
                                                    }
                                                }
                                            }
                                        }

                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                if (debugOn)
                                                {
                                                    debugRef.writeDebugMessage("Adding a BYE...", this.GetType().Name);
                                                }
                                                List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                                allLines.Add("000000 | BYE");
                                                allLines.Add("0");
                                                allLines.Add("0");
                                                allLines.Add("0");
                                                allLines.Add("0");
                                                allLines.Add("0");
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                        players.Add("000000 | BYE");
                                    }

                                    clearInformation(pairedPlayers);

                                    List<int> playerTiebreakers = new List<int>();

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Getting max tiebreaker...", this.GetType().Name);
                                    }

                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            string[] allLines = File.ReadAllLines(file.FullName);
                                            for (int i = 0; i < allLines.Length; i++)
                                            {
                                                for (int a = 0; a < players.Count; a++)
                                                {
                                                    if (allLines[i].Contains(players[a]))
                                                    {
                                                        playerTiebreakers.Add(Int32.Parse(allLines[i + 1]));
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    bool organizingPlayers = true;
                                    int counter = 0;
                                    int maxTiebreaker = playerTiebreakers.Max();

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Max tiebreaker is: " + maxTiebreaker, this.GetType().Name);
                                    }

                                    for (int i = 0; i < playerTiebreakers.Count; i++)
                                    {
                                        if (playerTiebreakers[i] == maxTiebreaker)
                                        {
                                            counter++;
                                        }
                                    }

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Organizing players by tiebreaker...", this.GetType().Name);
                                    }

                                    while (organizingPlayers)
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                if (playerTiebreakers.Count != 0)
                                                {
                                                    maxTiebreaker = playerTiebreakers.Max();
                                                    string[] allLines = File.ReadAllLines(file.FullName);
                                                    for (int i = 0; i < allLines.Length; i++)
                                                    {
                                                        for (int a = 0; a < players.Count; a++)
                                                        {
                                                            if (allLines[i].Contains(players[a]))
                                                            {
                                                                if (Int32.Parse(allLines[i + 1]) == maxTiebreaker)
                                                                {
                                                                    if (playersDropped.Count > 0)
                                                                    {
                                                                        for (int b = 0; b < playersDropped.Count; b++)
                                                                        {
                                                                            if (players[a].Contains(playersDropped[b]))
                                                                            {

                                                                            }
                                                                            else
                                                                            {
                                                                                playersByTb.Add(players[a]);
                                                                                playerTiebreakers.Remove(maxTiebreaker);
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        playersByTb.Add(players[a]);
                                                                        playerTiebreakers.Remove(maxTiebreaker);
                                                                        break;
                                                                    }
                                                                }
                                                            }

                                                        }

                                                    }
                                                }
                                                else
                                                {
                                                    organizingPlayers = false;
                                                }
                                            }
                                        }
                                    }
                                    roundNum_textBlock.Text = "Round: " + round;
                                    if (counter == 1 && !warned)
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("There is only one undefeated, opening Notice window to offer cut...", this.GetType().Name);
                                        }

                                        Notice noticeWin = new Notice();
                                        noticeWin.Owner = this;
                                        noticeWin.ShowDialog();
                                    }

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Pairing...", this.GetType().Name);
                                    }

                                    pair();
                                }
                                else
                                {
                                    round--;
                                }
                            }
                            else //If there are no players to drop, same logic but w/o dropping.
                            {
                                if (byeList.Items.Count > 0)
                                {
                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Adding results for BYE player: " + byeList.Items[0].ToString(), this.GetType().Name);
                                    }

                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            string[] allLines = File.ReadAllLines(file.FullName);
                                            for (int i = 0; i < allLines.Length; i++)
                                            {
                                                if (allLines[i].Contains(byeList.Items[0].ToString()))
                                                {
                                                    tieBreaker = Int32.Parse(allLines[i + 1]);
                                                    gameWins = Int32.Parse(allLines[i + 2]);
                                                    gameLosses = Int32.Parse(allLines[i + 3]);
                                                    matchWins = Int32.Parse(allLines[i + 4]);
                                                    matchLosses = Int32.Parse(allLines[i + 5]);

                                                    tieBreaker = tieBreaker + 1;

                                                    allLines[i + 1] = tieBreaker.ToString();
                                                    allLines[i + 2] = gameWins.ToString();
                                                    allLines[i + 3] = gameLosses.ToString();
                                                    allLines[i + 4] = matchWins.ToString();
                                                    allLines[i + 5] = matchLosses.ToString();
                                                }
                                            }
                                            File.WriteAllLines(file.FullName, allLines);
                                        }
                                    }
                                }
                                for (int i = 0; i < pairingList.Items.Count; i++)
                                {
                                    numResultsFound = 0;
                                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                                    roundInfo.Add("Table: ");
                                    roundInfo.Add(pph.table);
                                    roundInfo.Add(pph.playerOneName);
                                    roundInfo.Add(pph.playerTwoName);
                                    if (pph.g1 != "null" && !string.IsNullOrEmpty(pph.g1))
                                    {
                                        roundInfo.Add("G1: " + pph.g1);
                                    }
                                    else
                                    {
                                        roundInfo.Add("G1: NA");
                                    }
                                    if (pph.g2 != "null" && !string.IsNullOrEmpty(pph.g2))
                                    {
                                        roundInfo.Add("G2: " + pph.g2);
                                    }
                                    else
                                    {
                                        roundInfo.Add("G2: NA");
                                    }
                                    if (pph.g3 != "null" && !string.IsNullOrEmpty(pph.g3))
                                    {
                                        roundInfo.Add("G3: " + pph.g3);
                                    }
                                    else
                                    {
                                        roundInfo.Add("G3: NA");
                                    }
                                    roundInfo.Add("Results: ");
                                    roundInfo.Add(pph.results);

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Setting results for: " + pph.playerOneName + " vs " + pph.playerTwoName, this.GetType().Name);
                                    }

                                    if (pph.results.Substring(0, 1) == "2")
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int a = 0; a < allLines.Length; a++)
                                                {
                                                    if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage(pph.playerOneName + " won the match, updating results...", this.GetType().Name);
                                                        }

                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        tieBreaker = tieBreaker + 1;
                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                        matchWins++;

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage(pph.playerTwoName + " lost the match, updating results...", this.GetType().Name);
                                                        }

                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                        matchLosses++;

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (numResultsFound == 2)
                                                    {
                                                        break;
                                                    }
                                                }
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                    }
                                    else if (pph.results.Substring(2, 1) == "2")
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int a = 0; a < allLines.Length; a++)
                                                {
                                                    if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage(pph.playerTwoName + " won the match, updating results...", this.GetType().Name);
                                                        }

                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        tieBreaker = tieBreaker + 1;
                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                        matchWins++;

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage(pph.playerOneName + " lost the match, updating results...", this.GetType().Name);
                                                        }

                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                        matchLosses++;

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (numResultsFound == 2)
                                                    {
                                                        break;
                                                    }
                                                }
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                    }
                                    else if (pph.results == "1-1")
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Match was a draw, updating results...", this.GetType().Name);
                                        }

                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int a = 0; a < allLines.Length; a++)
                                                {
                                                    if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                    {
                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                    {
                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (numResultsFound == 2)
                                                    {
                                                        break;
                                                    }
                                                }
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //Uhhhh this isn't supposed to happen, something went wrong.
                                    }
                                }
                                if (byeList.Items.Count > 0)
                                {
                                    roundInfo.Add("BYE Player(s): ");
                                    roundInfo.Add(byeList.Items.GetItemAt(0).ToString());
                                }

                                if (!warned)
                                {
                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Updating our past pairings list...", this.GetType().Name);
                                    }
                                    for (int i = 0; i < pairingList.Items.Count; i++)
                                    {
                                        PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                                        pastPairings.Add(pph.playerOneName.Substring(0, 6) + pph.playerTwoName.Substring(0, 6));
                                        pastPairings.Add(pph.playerTwoName.Substring(0, 6) + pph.playerOneName.Substring(0, 6));
                                    }
                                }

                                if (debugOn)
                                {
                                    int roundRef = round - 1;
                                    debugRef.writeDebugMessage("Past pairings as of Round " + roundRef + ":", this.GetType().Name);
                                    for (int i = 0; i < pastPairings.Count; i++)
                                    {
                                        debugRef.writeDebugMessage(pastPairings[i].Substring(0, 6) + " vs " + pastPairings[i].Substring(6, 6), this.GetType().Name);
                                    }
                                }


                                round--;
                                filePath = path + "Round " + round + ".txt";
                                round++;
                                File.WriteAllLines(filePath, roundInfo);

                                clearInformation(pairedPlayers);

                                List<int> playerTiebreakers = new List<int>();

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Getting max tiebreaker...", this.GetType().Name);
                                }

                                foreach (var file in dir.GetFiles())
                                {
                                    if (file.Name == tournamentName + ".txt")
                                    {
                                        string[] allLines = File.ReadAllLines(file.FullName);
                                        for (int i = 0; i < allLines.Length; i++)
                                        {
                                            for (int a = 0; a < players.Count; a++)
                                            {
                                                if (allLines[i].Contains(players[a]))
                                                {
                                                    playerTiebreakers.Add(Int32.Parse(allLines[i + 1]));
                                                }
                                            }
                                        }
                                    }
                                }

                                bool organizingPlayers = true;
                                int counter = 0;
                                int maxTiebreaker = playerTiebreakers.Max(); //Bug is here

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Max tiebreaker is: " + maxTiebreaker, this.GetType().Name);
                                }

                                for (int i = 0; i < playerTiebreakers.Count; i++)
                                {
                                    if (playerTiebreakers[i] == maxTiebreaker)
                                    {
                                        counter++;
                                    }
                                }

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Organizing players by tiebreaker...", this.GetType().Name);
                                }

                                while (organizingPlayers)
                                {
                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            if (playerTiebreakers.Count != 0)
                                            {
                                                maxTiebreaker = playerTiebreakers.Max();
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int i = 0; i < allLines.Length; i++)
                                                {
                                                    for (int a = 0; a < players.Count; a++)
                                                    {
                                                        if (allLines[i].Contains(players[a]))
                                                        {
                                                            if (Int32.Parse(allLines[i + 1]) == maxTiebreaker)
                                                            {
                                                                if (playersDropped.Count > 0)
                                                                {
                                                                    for (int b = 0; b < playersDropped.Count; b++)
                                                                    {
                                                                        if (players[a].Contains(playersDropped[b]))
                                                                        {

                                                                        }
                                                                        else
                                                                        {
                                                                            playersByTb.Add(players[a]);
                                                                            playerTiebreakers.Remove(maxTiebreaker);
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    playersByTb.Add(players[a]);
                                                                    playerTiebreakers.Remove(maxTiebreaker);
                                                                    break;
                                                                }
                                                            }
                                                        }

                                                    }

                                                }
                                            }
                                            else
                                            {
                                                organizingPlayers = false;
                                            }
                                        }
                                    }
                                }
                                roundNum_textBlock.Text = "Round: " + round;
                                if (counter == 1 && !warned)
                                {
                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("There is only one undefeated, opening Notice window to offer cut...", this.GetType().Name);
                                    }

                                    Notice noticeWin = new Notice();
                                    noticeWin.Owner = this;
                                    noticeWin.ShowDialog();
                                }

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Pairing...", this.GetType().Name);
                                }

                                pair();

                            }
                        }
                        if (timeEntered > -1)
                        {
                            startTimer();
                            pausePlay_Button.Content = "⏸️";
                        }
                        else
                        {
                            //Do Nothing
                        }
                        if (pegasusFormat == true)
                        {
                            lastEvent = "Nothing yet!";
                            if (usedRules.Count > 0)
                            {
                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Leftovers in usedRules, resetting...", this.GetType().Name);
                                }

                                var rulesToReset = usedRules.Where(item => item.Length > 0).ToList();
                                usedRules = usedRules.Except(rulesToReset).ToList();
                                pegasusRules.AddRange(rulesToReset);

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("All usedRules should be returned to pegasusRules.", this.GetType().Name);
                                }

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("usedRules new count: " + usedRules.Count, this.GetType().Name);
                                }

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("pegasusRules new count: " + pegasusRules.Count, this.GetType().Name);
                                }
                            }
                            pStartTimer();
                            pausePlay_Button_Pegasus.Content = "⏸️";
                        }
                        else
                        {
                            //Do Nothing
                        }
                        break;
                    case false:
                        MessageBox.Show("Some matches are still unresolved\n" + "Enter all results before continuing", "Exception");
                        round--;
                        break;
                }
            }
        }

        private void pairingList_SelectedIndexChanged(object sender, EventArgs e)
        {
            PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
            if (pph != null)
            {
                table_textBlock.Text = pph.table;
                player1_textBox.Text = pph.playerOneName;
                player2_textBox.Text = pph.playerTwoName;
                p1Checked = 0;
                p2Checked = 0;
                g1p1_checkBox.IsChecked = false;
                g2p1_checkBox.IsChecked = false;
                g3p1_checkBox.IsChecked = false;
                g1p2_checkBox.IsChecked = false;
                g2p2_checkBox.IsChecked = false;
                g3p2_checkBox.IsChecked = false;
                draw_checkBox.IsChecked = false;
                p1Drop_checkBox.IsChecked = pph.p1IsDrop;
                p2Drop_checkBox.IsChecked = pph.p2IsDrop;
                draw_checkBox.IsChecked = pph.isDraw;

                if (pph.results != "0-0" && pph.isDraw != true)
                {
                    switch (pph.g1)
                    {
                        case "p1":
                            g1p1_checkBox.IsChecked = true;
                            p1Checked++;
                            break;
                        case "p2":
                            g1p2_checkBox.IsChecked = true;
                            p2Checked++;
                            break;
                    }

                    switch (pph.g2)
                    {
                        case "p1":
                            g2p1_checkBox.IsChecked = true;
                            p1Checked++;
                            break;
                        case "p2":
                            g2p2_checkBox.IsChecked = true;
                            p2Checked++;
                            break;
                    }

                    switch (pph.g3)
                    {
                        case "p1":
                            g3p1_checkBox.IsChecked = true;
                            p1Checked++;
                            break;
                        case "p2":
                            g3p2_checkBox.IsChecked = true;
                            p2Checked++;
                            break;
                    }
                }
            }
        }

        private void pair()
        {
            if (debugOn)
            {
                debugRef.writeDebugMessage("Pairing initialized...", this.GetType().Name);
            }

            if (!cutting)
            {
                bool firstRun = true;
                bool validatingPairings = true;
                while (validatingPairings)
                {
                    for (int i = 0; i < playersByTb.Count(); i++)
                    {
                        if (i % 2 == 0)
                        {
                            //Do Nothing
                        }
                        else
                        {
                            if (invalidPairing == true)
                            {
                                for (int a = 0; a < playersByTb.Count(); a++)
                                {

                                }
                                validatePairing(playersByTb[i], invalidPlayer, i);
                                invalidPairing = false;
                            }
                            else
                            {
                                if (firstRun == true)
                                {
                                    validatePairing(playersByTb[i], playersByTb[i - 1], i);
                                    firstRun = false;
                                }
                                else
                                {
                                    var whosMissing = playersByTb.Except(pairedPlayers).ToList();
                                    if (whosMissing.Any())
                                    {
                                        validatePairing(playersByTb[i], whosMissing[0], i);
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                        }
                    }
                    if (numPairingsCreated == players.Count / 2 && byeList.Items.Count < 2)
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("All players were successfully paired, listening...", this.GetType().Name);
                        }

                        validatingPairings = false;
                    }
                    else
                    {
                        numPairingsCreated = 0;
                        if (byeList.Items.Count > 1)
                        {
                            setInvalidPlayer(byeList.Items.GetItemAt(1).ToString());
                        }

                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("Something went wrong, repairing...", this.GetType().Name);
                        }

                        invalidPairing = true;
                    }
                }
            }
        }

        private void validatePairing(string playerTwo, string playerOne, int index)
        {
            if(!cutting)
            {
                bool failedCheckOne = false;
                bool failedCheckTwo = false;
                bool failedCheckThree = false;
                bool contValidation = true;
                while (contValidation == true)
                {
                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Validating Pairing: " + playerOne + " vs " + playerTwo, this.GetType().Name);
                    }

                    if (failedCheckOne == true && failedCheckTwo == true && failedCheckThree == true)
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("Failed all checks on " + playerOne + ", attempting again...", this.GetType().Name);
                        }
                        numPairingsCreated = 0;
                        pairedPlayers.Clear();
                        setInvalidPlayer(playerOne);
                        invalidPairing = true;
                        pairingList.Items.Clear();
                        byeList.Items.Clear();
                        contValidation = false;
                    }
                    else
                    {
                        bool matchFound = false;
                        for (int i = 0; i < pastPairings.Count; i++)
                        {
                            if (pastPairings[i].Contains(playerTwo.Substring(0, 6) + playerOne.Substring(0, 6))
                                || pastPairings[i].Contains(playerOne.Substring(0, 6) + playerTwo.Substring(0, 6)))
                            {
                                matchFound = true;
                                failedCheckOne = true;

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Failure: past pairing.", this.GetType().Name);
                                }
                            }
                            else
                            {
                                //Do Nothing
                            }
                        }

                        for (int i = 0; i < pairedPlayers.Count; i++)
                        {
                            if (pairedPlayers[i].Contains(playerOne) || pairedPlayers[i].Contains(playerTwo))
                            {
                                matchFound = true;
                                failedCheckTwo = true;

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Failure: player already paired.", this.GetType().Name);
                                }
                                break;
                            }
                            else
                            {

                            }
                        }

                        if (playerOne == playerTwo)
                        {
                            matchFound = true;
                            failedCheckThree = true;

                            if (debugOn)
                            {
                                debugRef.writeDebugMessage("Failure: duplicate name.", this.GetType().Name);
                            }
                        }

                        if (matchFound == false)
                        {
                            if (playerOne.Contains("000000 | BYE") || playerTwo.Contains("000000 | BYE"))
                            {
                                if (!playerOne.Contains("000000 | BYE"))
                                {
                                    byeList.Items.Add(playerOne);
                                }
                                else
                                {
                                    byeList.Items.Add(playerTwo);
                                }
                            }
                            else
                            {
                                pairingList.Items.Add(new PlayerPairingHandler
                                {
                                    playerTwoName = playerTwo,
                                    playerOneName = playerOne,
                                    results = "0-0",
                                    table = table.ToString(),
                                    vs = "vs",
                                });
                            }
                            pairedPlayers.Add(playerOne);
                            pairedPlayers.Add(playerTwo);
                            if (playerOne.Contains("000000 | BYE") || playerTwo.Contains("000000 | BYE"))
                            {
                                //Don't Increment Table
                            }
                            else
                            {
                                table++;
                            }
                            numPairingsCreated++;
                            if (debugOn)
                            {
                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Succesfully paired players.", this.GetType().Name);
                                }
                            }
                            contValidation = false;
                        }
                        else
                        {
                            if (index < playersByTb.Count - 1)
                            {
                                index++;
                            }
                            else
                            {
                                index = 0;
                            }
                            playerTwo = getNewPairing(index);
                        }
                    }
                }
            }
        }

        private void validatePlayer(BindingList<string> list)
        {
            Random rand = new Random();
            string player = players[rand.Next(0, players.Count())];
            bool contValidation = true;

            if (debugOn)
            {
                debugRef.writeDebugMessage("Checking if " + player + " is valid to be paired...", this.GetType().Name);
            }

            if (round == 1)
            {
                while (contValidation == true)
                {
                    bool matchFound = false;
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (player == list[i])
                        {
                            matchFound = true;
                            break;
                        }
                        else
                        {
                            //Do Nothing
                        }
                    }

                    if (!matchFound)
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage(player + " was valid, adding to list pairedPlayers.", this.GetType().Name);
                        }
                        pairedPlayers.Add(player);
                        contValidation = false;
                        break;
                    }
                    else
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage(player + " was not valid, getting a new player.", this.GetType().Name);
                        }
                        player = getNewPlayer();
                    }
                }
            }
        }

        string getNewPlayer()
        {
            Random rand = new Random();
            string newPlayer = players[rand.Next(0, players.Count())];
            return newPlayer;
        }

        string getNewPairing(int index)
        {
            string newPlayer;
            newPlayer = playersByTb[index];
            return newPlayer;
        }

        private void setInvalidPlayer(string player)
        {
            if (debugOn)
            {
                debugRef.writeDebugMessage("Setting " + player + " to invalid for failing checks.", this.GetType().Name);
            }

            invalidPlayer = player;
        }

        public void clearInformation(BindingList<string> list)
        {
            if (debugOn)
            {
                debugRef.writeDebugMessage("Clearing all relevant information...", this.GetType().Name);
            }

            filePath = path + tournamentName + ".txt";
            table_textBlock.Text = "";
            player1_textBox.Text = "";
            player2_textBox.Text = "";
            p1Checked = 0;
            p2Checked = 0;
            numPairingsCreated = 0;
            g1p1_checkBox.IsChecked = false;
            g2p1_checkBox.IsChecked = false;
            g3p1_checkBox.IsChecked = false;
            g1p2_checkBox.IsChecked = false;
            g2p2_checkBox.IsChecked = false;
            g3p2_checkBox.IsChecked = false;
            draw_checkBox.IsChecked = false;
            p1Drop_checkBox.IsChecked = false;
            p2Drop_checkBox.IsChecked = false;
            dropBye_checkBox.IsChecked = false;
            dropBye = false;
            if (!cutting)
            {
                playersByTb.Clear();
            }
            if (timeEntered > -1)
            {
                pausePlay_Button.Content = "⏸️";
            }
            if (pegasusFormat == true)
            {
                pausePlay_Button_Pegasus.Content = "⏸️";
            }
            byeList.Items.Clear();
            pairingList.Items.Clear();
            list.Clear();
            //pairingList.SelectedItems.Clear();
        }

        public void clearPairingHistory()
        {
            pastPairings.Clear();
            warned = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (seconds <= 0)
            {
                if (seconds == 0 && minutes == 0)
                {
                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Times up, opening Time window...", this.GetType().Name);
                    }

                    timer1.Stop();
                    Time timeWin = new Time();
                    timeWin.Owner = this;
                    timeWin.Show();
                }
                else
                {
                    seconds = 59;
                    minutes = minutes - 1;
                }
            }
            else
            {
                seconds = seconds - 1;
            }

            minutesStr = minutes.ToString();

            if (seconds <= 9)
            {
                secondsStr = "0" + seconds.ToString();
            }
            else
            {
                secondsStr = seconds.ToString();
            }

            timer_textBlock.Text = minutesStr + ":" + secondsStr;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (pSeconds <= 0)
            {
                if (pSeconds == 0 && pMinutes == 0)
                {
                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Pegasus timer is up, opening PegasusEvent window...", this.GetType().Name);
                    }

                    timer2.Stop();
                    if(pegasusRules.Count > 0)
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("pegasusRules list still has values, continuing...", this.GetType().Name);
                        }
                        PegasusEvent pegEveWin = new PegasusEvent();
                        pegEveWin.Owner = this;
                        pegEveWin.initialSetup();
                        pegEveWin.Show();
                    }
                    else
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("pegasusRules list does not have values anymore, resetting...", this.GetType().Name);
                        }

                        var rulesToReset = usedRules.Where(item => item.Length > 0).ToList();
                        usedRules = usedRules.Except(rulesToReset).ToList();
                        pegasusRules.AddRange(rulesToReset);

                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("All usedRules should be returned to pegasusRules.", this.GetType().Name);
                        }

                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("usedRules new count: " + usedRules.Count, this.GetType().Name);
                        }

                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("pegasusRules new count: " + pegasusRules.Count, this.GetType().Name);
                        }

                        PegasusEvent pegEveWin = new PegasusEvent();
                        pegEveWin.Owner = this;
                        pegEveWin.initialSetup();
                        pegEveWin.Show();
                    }

                }
                else
                {
                    pSeconds = 59;
                    pMinutes = pMinutes - 1;
                }
            }
            else
            {
                pSeconds = pSeconds - 1;
            }

            pMinutesStr = pMinutes.ToString();

            if (pSeconds <= 9)
            {
                pSecondsStr = "0" + pSeconds.ToString();
            }
            else
            {
                pSecondsStr = pSeconds.ToString();
            }

            pFormTimer_textBlock.Text = pMinutesStr + ":" + pSecondsStr;
        }

        /*Game Checkbox Functionality*/
        private void g1p1_OnClick(object sender, RoutedEventArgs e)
        {
            if (p1Checked <= 2)
            {
                if (pairingList.SelectedItem != null)
                {
                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
                    if (draw_checkBox.IsChecked == true)
                    {
                        draw_checkBox.IsChecked = false;
                        pph.results = "0-0";
                        pph.isDraw = false;
                    }

                    if (g1p2_checkBox.IsChecked == true && p1Checked != 2)
                    {
                        g1p2_checkBox.IsChecked = false;
                        removeResults(2);
                    }

                    if (g1p1_checkBox.IsChecked == false)
                    {
                        pph.g1 = "null";
                        removeResults(1);
                    }
                    else
                    {
                        if (p1Checked != 2)
                        {
                            pph.g1 = "p1";
                            addResults(1);
                        }
                        else
                        {
                            g1p1_checkBox.IsChecked = false;
                        }
                    }

                    
                }
                else
                {
                    g1p1_checkBox.IsChecked = false;
                }
            }
            else
            {
                g1p1_checkBox.IsChecked = false;
            }
        }

        private void g2p1_OnClick(object sender, RoutedEventArgs e)
        {
            if (p1Checked <= 2)
            {
                if (pairingList.SelectedItem != null)
                {
                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
                    if (draw_checkBox.IsChecked == true)
                    {
                        draw_checkBox.IsChecked = false;
                        pph.results = "0-0";
                        pph.isDraw = false;
                    }

                    if (g2p2_checkBox.IsChecked == true && p1Checked != 2)
                    {
                        g2p2_checkBox.IsChecked = false;
                        removeResults(2);
                    }

                    if (g2p1_checkBox.IsChecked == false)
                    {
                        pph.g2 = "null";
                        removeResults(1);
                    }
                    else
                    {
                        if (p1Checked != 2)
                        {
                            pph.g2 = "p1";
                            addResults(1);
                        } 
                        else
                        {
                            g2p1_checkBox.IsChecked = false;
                        }
                    }
                }
                else
                {
                    g2p1_checkBox.IsChecked = false;
                }
            }
            else
            {
                g2p1_checkBox.IsChecked = false;
            }
        }

        private void g3p1_OnClick(object sender, RoutedEventArgs e)
        {
            if (p1Checked <= 2)
            {
                if (pairingList.SelectedItem != null)
                {
                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
                    if (draw_checkBox.IsChecked == true)
                    {
                        draw_checkBox.IsChecked = false;
                        pph.results = "0-0";
                        pph.isDraw = false;
                    }

                    if (g3p2_checkBox.IsChecked == true && p1Checked != 2)
                    {
                        g3p2_checkBox.IsChecked = false;
                        removeResults(2);
                    }

                    if (g3p1_checkBox.IsChecked == false)
                    {
                        pph.g3 = "null";
                        removeResults(1);
                    }
                    else
                    {
                        if (p1Checked != 2)
                        {
                            pph.g3 = "p1";
                            addResults(1);
                        }
                        else
                        {
                            g3p1_checkBox.IsChecked = false;
                        }
                    }
                }
                else
                {
                    g3p1_checkBox.IsChecked = false;
                }
            }
            else
            {
                g3p1_checkBox.IsChecked = false;
            }
        }

        private void g1p2_OnClick(object sender, RoutedEventArgs e)
        {
            if (p2Checked <= 2)
            {
                if (pairingList.SelectedItem != null)
                {
                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
                    if (draw_checkBox.IsChecked == true)
                    {
                        draw_checkBox.IsChecked = false;
                        pph.results = "0-0";
                        pph.isDraw = false;
                    }

                    if (g1p1_checkBox.IsChecked == true && p2Checked != 2)
                    {
                        g1p1_checkBox.IsChecked = false;
                        removeResults(1);
                    }

                    if (g1p2_checkBox.IsChecked == false)
                    {
                        pph.g1 = "null";
                        removeResults(2);
                    }
                    else
                    {
                        if (p2Checked != 2)
                        {
                            pph.g1 = "p2";
                            addResults(2);
                        }
                        else
                        {
                            g1p2_checkBox.IsChecked = false;
                        }
                    }
                }
                else
                {
                    g1p2_checkBox.IsChecked = false;
                }
            }
            else
            {
                g1p2_checkBox.IsChecked = false;
            }
        }

        private void g2p2_OnClick(object sender, RoutedEventArgs e)
        {
            if (p2Checked <= 2)
            {
                if (pairingList.SelectedItem != null)
                {
                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
                    if (draw_checkBox.IsChecked == true)
                    {
                        draw_checkBox.IsChecked = false;
                        pph.results = "0-0";
                        pph.isDraw = false;
                    }

                    if (g2p1_checkBox.IsChecked == true && p2Checked != 2)
                    {
                        g2p1_checkBox.IsChecked = false;
                        removeResults(1);
                    }

                    if (g2p2_checkBox.IsChecked == false)
                    {
                        pph.g2 = "null";
                        removeResults(2);
                    }
                    else
                    {
                        if (p2Checked != 2)
                        {
                            pph.g2 = "p2";
                            addResults(2);
                        }
                        else
                        {
                            g2p2_checkBox.IsChecked = false;
                        }
                    }
                }
                else
                {
                    g2p2_checkBox.IsChecked = false;
                }
            }
            else
            {
                g2p2_checkBox.IsChecked = false;
            }
        }

        private void g3p2_OnClick(object sender, RoutedEventArgs e)
        {
            if(p2Checked <= 2)
            {
                if (pairingList.SelectedItem != null)
                {
                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
                    if (draw_checkBox.IsChecked == true)
                    {
                        draw_checkBox.IsChecked = false;
                        pph.results = "0-0";
                        pph.isDraw = false;
                    }

                    if (g3p1_checkBox.IsChecked == true && p2Checked != 2)
                    {
                        g3p1_checkBox.IsChecked = false;
                        removeResults(1);
                    }

                    if (g3p2_checkBox.IsChecked == false)
                    {
                        pph.g3 = "null";
                        removeResults(2);
                    }
                    else
                    {
                        if (p2Checked != 2)
                        {
                            pph.g3 = "p2";
                            addResults(2);
                        }
                        else
                        {
                            g3p2_checkBox.IsChecked = false;
                        }
                    }
                }
                else
                {
                    g3p2_checkBox.IsChecked = false;
                }
            }
            else
            {
                g3p2_checkBox.IsChecked = false;
            }
        }

        private void draw_OnClick(object sender, RoutedEventArgs e)
        {
            if (pairingList.SelectedItem != null && !cutting)
            {
                PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
                if (draw_checkBox.IsChecked == true)
                {
                    p1Checked = 0;
                    p2Checked = 0;
                    g1p1_checkBox.IsChecked = false;
                    g2p1_checkBox.IsChecked = false;
                    g3p1_checkBox.IsChecked = false;
                    g1p2_checkBox.IsChecked = false;
                    g2p2_checkBox.IsChecked = false;
                    g3p2_checkBox.IsChecked = false;
                    pph.results = "1-1";
                    pph.isDraw = true;
                    pairingList.Items.Refresh();
                }
                else
                {
                    draw_checkBox.IsChecked = false;
                    pph.results = "0-0";
                    pairingList.Items.Refresh();
                }
            }
            else
            {
                if (cutting) 
                {
                    MessageBox.Show("You can't draw in top\n", "Exception");
                }
                draw_checkBox.IsChecked = false;
            }
        }
        /*End Game Checkbox Functionality*/

        private void addResults(int player)
        {
            int resultToAdd;
            PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;

            switch (player)
            {
                case 1:
                    resultToAdd = Int32.Parse(pph.results.Substring(0, 1));
                    if (resultToAdd < 2)
                    {
                        resultToAdd = resultToAdd + 1;
                        pph.results = resultToAdd.ToString() + pph.results.Substring(1, 2);
                        pairingList.Items.Refresh();
                        p1Checked++;
                    }
                    break;
                case 2:
                    resultToAdd = Int32.Parse(pph.results.Substring(2, 1));
                    if (resultToAdd < 2)
                    {
                        resultToAdd = resultToAdd + 1;
                        pph.results = pph.results.Substring(0, 2) + resultToAdd.ToString();
                        pairingList.Items.Refresh();
                        p2Checked++;
                    }
                    break;
            }
        }

        private void removeResults(int player)
        {
            int resultToAdd;
            PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;

            switch (player)
            {
                case 1:
                    resultToAdd = Int32.Parse(pph.results.Substring(0, 1));
                    if (resultToAdd <= 2)
                    {
                        resultToAdd = resultToAdd - 1;
                        pph.results = resultToAdd.ToString() + pph.results.Substring(1, 2);
                        pairingList.Items.Refresh();
                        p1Checked--;
                    }
                    break;
                case 2:
                    resultToAdd = Int32.Parse(pph.results.Substring(2, 1));
                    if (resultToAdd <= 2)
                    {
                        resultToAdd = resultToAdd - 1;
                        pph.results = pph.results.Substring(0, 2) + resultToAdd.ToString();
                        pairingList.Items.Refresh();
                        p2Checked--;
                    }
                    break;
            }
        }

        private void dropP1_OnClick(object sender, RoutedEventArgs e)
        {
            if (pairingList.SelectedItem != null && !cutting)
            {
                PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
                if (p1Drop_checkBox.IsChecked == true)
                {
                    playersToDrop.Add(player1_textBox.Text);
                    pph.p1IsDrop = true;
                }
                else
                {
                    playersToDrop.Remove(player1_textBox.Text);
                    pph.p1IsDrop = false;
                }
            }
            else
            {
                if (cutting)
                {
                    MessageBox.Show("You can't drop players in top\n" + "The loser will be removed automatically", "Exception");
                }
                p1Drop_checkBox.IsChecked = false;
            }
        }

        private void dropP2_OnClick(object sender, RoutedEventArgs e)
        {
            if (pairingList.SelectedItem != null && !cutting)
            {
                PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;
                if (p2Drop_checkBox.IsChecked == true)
                {
                    playersToDrop.Add(player2_textBox.Text);
                    pph.p2IsDrop = true;
                }
                else
                {
                    playersToDrop.Remove(player2_textBox.Text);
                    pph.p2IsDrop = false;
                }
            }
            else
            {
                if (cutting)
                {
                    MessageBox.Show("You can't drop players in top\n" + "The loser will be removed automatically", "Exception");
                }
                p2Drop_checkBox.IsChecked = false;
            }
        }

        private void dropBye_OnClick(object sender, RoutedEventArgs e)
        {
            if (byeList.Items.Count > 0)
            {
                if (dropBye_checkBox.IsChecked == true)
                {
                    playersToDrop.Add(byeList.Items.GetItemAt(0).ToString());
                }
                else
                {
                    playersToDrop.Remove(byeList.Items.GetItemAt(0).ToString());
                }
            }
            else
            {
                dropBye_checkBox.IsChecked = false;
            }
        }

        private void adjustPairings_OnClick(object sender, RoutedEventArgs e)
        {
            bool resultsEntered = false;
            bool byeListChecked = false;
            for (int i = 0; i < pairingList.Items.Count; i++)
            {
                PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                if(pph.results != "0-0")
                {
                    resultsEntered = true;
                }
            }
            if (pairingList.Items.Count > 0 && !resultsEntered)
            {
                if (debugOn)
                {
                    debugRef.writeDebugMessage("Opening the Adjust Pairings window...", this.GetType().Name);
                }
                var doesWinExist = Application.Current.Windows.OfType<AdjustPairings>().FirstOrDefault();
                if (doesWinExist == null)
                {
                    AdjustPairings adjWin = new AdjustPairings();
                    adjWin.Owner = this;
                    adjWin.Show();
                    adjWin.tournamentName = tournamentName;
                    if (byeList.Items.Count > 0)
                    {
                        adjWin.originalCount = pairingList.Items.Count + 1;
                    }
                    else
                    {
                        adjWin.originalCount = pairingList.Items.Count;
                    }
                    for (int i = 0; i < pairingList.Items.Count; i++)
                    {
                        PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                        adjWin.pairingList.Items.Add(new PlayerPairingHandler
                        {
                            playerTwoName = pph.playerTwoName,
                            playerOneName = pph.playerOneName,
                            table = pph.table,
                            vs = "vs",
                        });
                        if (byeList.Items.Count > 0 && !byeListChecked && i == pairingList.Items.Count - 1)
                        {
                            adjWin.pairingList.Items.Add(new PlayerPairingHandler
                            {
                                playerTwoName = "000000 | BYE",
                                playerOneName = byeList.Items.GetItemAt(0).ToString(),
                                table = "0",
                                vs = "vs",
                            });
                            byeListChecked = true;
                        }
                    }
                }
                else
                {
                    debugRef.writeDebugMessage("Adjust Pairings window is already open, bringing it to front...", this.GetType().Name);
                    doesWinExist.Activate();
                }
            }
            else
            {
                if (resultsEntered)
                {
                    MessageBox.Show("You must clear all results\n" + "Before adjusting pairings", "Exception");
                }
            }
        }

        private void roundEdit_OnClick(object sender, RoutedEventArgs e)
        {
            bool resultsEntered = false;
            for (int i = 0; i < pairingList.Items.Count; i++)
            {
                PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                if (pph.results != "0-0")
                {
                    resultsEntered = true;
                }
            }
            if(!cutting)
            {
                if (pairingList.Items.Count > 0 && !resultsEntered)
                {
                    var doesWinExist = Application.Current.Windows.OfType<RoundEdit>().FirstOrDefault();
                    if (doesWinExist == null)
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("Opening the Round Edit window...", this.GetType().Name);
                        }

                        RoundEdit rndedWin = new RoundEdit();
                        rndedWin.Owner = this;
                        rndedWin.Show();
                        rndedWin.tournamentName = tournamentName;
                        foreach (var file in dir.GetFiles())
                        {
                            if (file.Name.Contains("Round"))
                            {
                                rndedWin.rounds.Add(file.Name.Trim(new char[] { '.', 't', 'x', 't' }));
                                rndedWin.round_listBox.Items.Add(file.Name.Trim(new char[] { '.', 't', 'x', 't' }));
                            }
                        }
                        rndedWin.round_listBox.Items.Refresh();
                    }
                    else
                    {
                        debugRef.writeDebugMessage("Round Edit window is already open, bringing it to front...", this.GetType().Name);
                        doesWinExist.Activate();
                    }
                }
                else
                {
                    if (resultsEntered)
                    {
                        MessageBox.Show("You must clear all results\n" + "Before editing round(s)", "Exception");
                    }
                }
            }
            else
            {
                var doesWinExist = Application.Current.Windows.OfType<RoundEdit>().FirstOrDefault();
                if (doesWinExist == null)
                {
                    if (!reWarning)
                    {
                        MessageBox.Show("Since the tournaments cut to top\n" + "You'll only be able to return to top rounds\n", "Note");
                        reWarning = true;
                    }
                    else
                    {

                    }
                    if (pairingList.Items.Count > 0 && !resultsEntered)
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("Opening the Round Edit window...", this.GetType().Name);
                        }

                        RoundEdit rndedWin = new RoundEdit();
                        rndedWin.Owner = this;
                        rndedWin.Show();
                        rndedWin.tournamentName = tournamentName;
                        foreach (var file in dir.GetFiles())
                        {
                            if (file.Name.Contains("(Cut)Round"))
                            {
                                rndedWin.rounds.Add(file.Name.Trim(new char[] { '.', 't', 'x', 't' }));
                                rndedWin.round_listBox.Items.Add(file.Name.Trim(new char[] { '.', 't', 'x', 't' }));
                            }
                        }
                        rndedWin.round_listBox.Items.Refresh();
                    }
                    else
                    {
                        if (resultsEntered)
                        {
                            MessageBox.Show("You must clear all results\n" + "Before editing round(s)", "Exception");
                        }
                    }
                }
                else
                {
                    debugRef.writeDebugMessage("Round Edit window is already open, bringing it to front...", this.GetType().Name);
                    doesWinExist.Activate();
                }
            }
        }

        public void updateAdjustedPairings(List<string> list)
        {
            if (debugOn)
            {
                debugRef.writeDebugMessage("Resetting all GUI elements...", this.GetType().Name);
            }

            table_textBlock.Text = "";
            player1_textBox.Text = "";
            player2_textBox.Text = "";
            p1Checked = 0;
            p2Checked = 0;
            g1p1_checkBox.IsChecked = false;
            g2p1_checkBox.IsChecked = false;
            g3p1_checkBox.IsChecked = false;
            g1p2_checkBox.IsChecked = false;
            g2p2_checkBox.IsChecked = false;
            g3p2_checkBox.IsChecked = false;
            draw_checkBox.IsChecked = false;
            p1Drop_checkBox.IsChecked = false;
            p2Drop_checkBox.IsChecked = false;
            dropBye_checkBox.IsChecked = false;
            dropBye = false;

            if (debugOn)
            {
                debugRef.writeDebugMessage("Restarting timer...", this.GetType().Name);
            }

            if (timeEntered > -1)
            {
                startTimer();
            }
            else
            {
                //Do Nothing
            }

            if (pegasusFormat == true)
            {
                pStartTimer();
            }
            else
            {
                //Do Nothing
            }

            if (debugOn)
            {
                debugRef.writeDebugMessage("Cleaning our pastPairings list of all newly invalidated past pairings...", this.GetType().Name);
            }

            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    cleanList(pastPairings, list[i]);
                }
            }
        }

        public void cleanList(List<string> list, string element)
        {
            while (list.Contains(element))
            {
                list.Remove(element);
            }
        }

        private void debug_OnClick(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(dbPath + @"\Debug\"))
            {
                //Do Nothing
            }
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(dbPath + @"\Debug\");
            }

            if (debug_checkBox.IsChecked == true)
            {
                if(!dbgFileCreated)
                {
                    dbgName = "Debug_" + tournamentName + "_" + DateTime.Today.ToString("MMddyyy") + ".txt";
                    dbgFilePath = dbPath + @"\Debug\" + dbgName;
                    File.WriteAllText(dbgFilePath, "Debug Start: ");
                    dbgFileCreated = true;
                }

                debugOn = true;
                Debug debugWin = new Debug();
                debugRef = debugWin;
                debugWin.Owner = this;
                debugWin.buildListBox();
                debugWin.Show();
            }
            else
            {
                debugOn = false;

                if (debugRef.IsEnabled)
                {
                    debugRef.Close();
                }
            }
        }

        private void cut_OnClick(object sender, RoutedEventArgs e)
        {
            if (round >= 2)
            {
                if(!cutting)
                {
                    var doesWinExist = Application.Current.Windows.OfType<Cut>().FirstOrDefault();
                    if (doesWinExist == null)
                    {
                        if (debugOn)
                        {
                            debugRef.writeDebugMessage("Opening Cut window...", this.GetType().Name);
                        }
                        Cut cutWin = new Cut();
                        cutWin.Owner = this;
                        cutWin.Show();
                    }
                    else
                    {
                        debugRef.writeDebugMessage("Cut window is already open, bringing it to front...", this.GetType().Name);
                        doesWinExist.Activate();
                    }
                }
                else
                {
                    debugRef.writeDebugMessage("Tried to open Cut window but we've already cut to top, stopping execute...", this.GetType().Name);
                }
            }
            else
            {
                MessageBox.Show("Try playing a round before cutting");
            }
        }

        private void playerList_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<PlayerList>().FirstOrDefault();
            if (doesWinExist == null)
            {
                PlayerList plWin = new PlayerList();
                plWin.Owner = this;
                List<int> playerTiebreakers = new List<int>();
                List<string> playersByTbRef = new List<string>();

                foreach (var file in dir.GetFiles())
                {
                    if (file.Name == tournamentName + ".txt")
                    {
                        string[] allLines = File.ReadAllLines(file.FullName);
                        for (int i = 0; i < allLines.Length; i++)
                        {
                            for (int a = 0; a < players.Count; a++)
                            {
                                if (allLines[i].Contains(players[a]))
                                {
                                    playerTiebreakers.Add(Int32.Parse(allLines[i + 1]));
                                }
                            }
                        }
                    }
                }

                bool organizingPlayers = true;
                int maxTiebreaker = playerTiebreakers.Max();

                if (debugOn)
                {
                    debugRef.writeDebugMessage("Organizing players by tiebreaker...", this.GetType().Name);
                }

                while (organizingPlayers)
                {
                    foreach (var file in dir.GetFiles())
                    {
                        if (file.Name == tournamentName + ".txt")
                        {
                            if (playerTiebreakers.Count != 0)
                            {
                                maxTiebreaker = playerTiebreakers.Max();
                                string[] allLines = File.ReadAllLines(file.FullName);
                                for (int i = 0; i < allLines.Length; i++)
                                {
                                    for (int a = 0; a < players.Count; a++)
                                    {
                                        if (allLines[i].Contains(players[a]))
                                        {
                                            if (Int32.Parse(allLines[i + 1]) == maxTiebreaker)
                                            {
                                                if (playersDropped.Count > 0)
                                                {
                                                    for (int b = 0; b < playersDropped.Count; b++)
                                                    {
                                                        if (players[a].Contains(playersDropped[b]))
                                                        {

                                                        }
                                                        else
                                                        {
                                                            playersByTbRef.Add(players[a]);
                                                            playerTiebreakers.Remove(maxTiebreaker);
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    playersByTbRef.Add(players[a]);
                                                    playerTiebreakers.Remove(maxTiebreaker);
                                                    break;
                                                }
                                            }
                                        }

                                    }

                                }
                            }
                            else
                            {
                                organizingPlayers = false;
                            }
                        }
                    }
                }
                foreach (var file in dir.GetFiles())
                {
                    if (file.Name == tournamentName + ".txt")
                    {
                        string[] allLines = File.ReadAllLines(file.FullName);
                        for (int i = 0; i < playersByTbRef.Count; i++)
                        {
                            for (int a = 0; a < allLines.Length; a++)
                            {
                                if (allLines[a].Contains(playersByTbRef[i]) && !playersByTbRef[i].Contains("000000 | BYE"))
                                {
                                    plWin.playerList.Items.Add(new PlayerInfoHandler
                                    {
                                        name = playersByTbRef[i],
                                        gamesRecord = allLines[a + 2] + "-" + allLines[a + 3],
                                        matchesRecord = allLines[a + 4] + "-" + allLines[a + 5],
                                        tiebreaker = allLines[a + 1],
                                    });
                                }
                                else
                                {

                                }
                            }
                        }
                    }
                }
                plWin.playerList.Items.Refresh();
                plWin.Show();
            }
            else
            {
                debugRef.writeDebugMessage("Player List window is already open, bringing it to front...", this.GetType().Name);
                doesWinExist.Activate();
            }
            
        }

        public void cut(int numPlayers)
        {
            if (debugOn)
            {
                debugRef.writeDebugMessage("Cutting...", this.GetType().Name);
            }

            int playersAdded = 0;

            if (!split)
            {
                if (warned) 
                {
                    table = 1;
                    numCut = numPlayers;
                    cutting = true;
                    clearInformation(pairedPlayers);
                    playersByTb.Clear();

                    List<int> playerTiebreakers = new List<int>();

                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Getting max tiebreaker...", this.GetType().Name);
                    }

                    foreach (var file in dir.GetFiles())
                    {
                        if (file.Name == tournamentName + ".txt")
                        {
                            string[] allLines = File.ReadAllLines(file.FullName);
                            for (int i = 0; i < allLines.Length; i++)
                            {
                                for (int a = 0; a < players.Count; a++)
                                {
                                    if (allLines[i].Contains(players[a]))
                                    {
                                        playerTiebreakers.Add(Int32.Parse(allLines[i + 1]));
                                    }
                                }
                            }
                        }
                    }

                    bool organizingPlayers = true;
                    int maxTiebreaker = playerTiebreakers.Max();

                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Max tiebreaker is: " + maxTiebreaker, this.GetType().Name);
                    }

                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Organizing players by tiebreaker...", this.GetType().Name);
                    }

                    while (organizingPlayers)
                    {
                        foreach (var file in dir.GetFiles())
                        {
                            if (file.Name == tournamentName + ".txt")
                            {
                                if (playerTiebreakers.Count != 0)
                                {
                                    maxTiebreaker = playerTiebreakers.Max();
                                    string[] allLines = File.ReadAllLines(file.FullName);
                                    for (int i = 0; i < allLines.Length; i++)
                                    {
                                        for (int a = 0; a < players.Count; a++)
                                        {
                                            if (allLines[i].Contains(players[a]))
                                            {
                                                if (Int32.Parse(allLines[i + 1]) == maxTiebreaker)
                                                {
                                                    if (playersDropped.Count > 0)
                                                    {
                                                        for (int b = 0; b < playersDropped.Count; b++)
                                                        {
                                                            if (players[a].Contains(playersDropped[b]))
                                                            {

                                                            }
                                                            else
                                                            {
                                                                playersByTb.Add(players[a]);
                                                                playerTiebreakers.Remove(maxTiebreaker);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        playersByTb.Add(players[a]);
                                                        playerTiebreakers.Remove(maxTiebreaker);
                                                        break;
                                                    }
                                                }
                                            }

                                        }

                                    }
                                }
                                else
                                {
                                    organizingPlayers = false;
                                }
                            }
                        }
                    }

                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Clearing pastPairings list...", this.GetType().Name);
                    }

                    pastPairings.Clear();

                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Cutting to top...", this.GetType().Name);
                    }

                    if (excludeJudgeOrg == true)
                    {
                        foreach (var file in dbDir.GetFiles())
                        {
                            if (file.FullName.Contains("PegasusRules"))
                            {

                            }
                            else
                            {
                                if (File.ReadLines(file.FullName).Skip(7).Take(1).First().Equals("Yes"))
                                {
                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Excluding " + File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First() + " as they are J/O.", this.GetType().Name);
                                    }

                                    cleanList(playersByTb, File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First());
                                }
                            }
                        }
                    }

                    for (int i = 0; i < playersByTb.Count; i++)
                    {
                        if(playersAdded < numPlayers)
                        {
                            topCut.Add(compareScores(playersByTb[i], numPlayers));
                            playersAdded = playersAdded + 1;
                        }

                        if (wasPlayerRemoved)
                        {
                            i = i - 1;
                            wasPlayerRemoved = false;
                        }
                    }

                    for (int i = 0; i < topCut.Count; i++)
                    {
                        if (i % 2 == 0)
                        {
                            pairingList.Items.Add(new PlayerPairingHandler
                            {
                                playerTwoName = topCut[i + 1],
                                playerOneName = topCut[i],
                                results = "0-0",
                                table = table.ToString(),
                                vs = "vs",
                            });
                            table++;
                        }
                    }

                    if (debugOn)
                    {
                        debugRef.writeDebugMessage("Top cut is: ", this.GetType().Name);
                        for(int i = 0; i < topCut.Count; i++)
                        {
                            debugRef.writeDebugMessage(topCut[i], this.GetType().Name);
                        }
                    }

                    roundNum_textBlock.Text = "Round: " + round + " (Cut)";
                }
                else
                {
                    bool resultsIn = true;
                    for (int i = 0; i < pairingList.Items.Count; i++)
                    {
                        PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                        if (pph.results == "0-0" || pph.results == "1-0" || pph.results == "0-1")
                        {
                            resultsIn = false;
                        }
                    }

                    switch (resultsIn)
                    {
                        case true:
                            table = 1;
                            numCut = numPlayers;
                            cutting = true;
                            int numResultsFound = 0;
                            if (playersToDrop.Count > 0)
                            {
                                string dropPlayersDisp = "";
                                for (int i = 0; i < playersToDrop.Count; i++)
                                {
                                    dropPlayersDisp = dropPlayersDisp + "\n" + playersToDrop[i];
                                }
                                MessageBoxResult confirmResult = MessageBox.Show("You're about to drop\n" + dropPlayersDisp + "\n\nAre you sure?", "Confirmation Box", MessageBoxButton.YesNo);
                                if (confirmResult == MessageBoxResult.Yes)
                                {
                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Dropping players has been confirmed...", this.GetType().Name);
                                    }
                                    if (byeList.Items.Count > 0)
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Removing BYE...", this.GetType().Name);
                                        }
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int i = 0; i < allLines.Length; i++)
                                                {
                                                    if (allLines[i].Contains(byeList.Items[0].ToString()))
                                                    {
                                                        tieBreaker = Int32.Parse(allLines[i + 1]);
                                                        gameWins = Int32.Parse(allLines[i + 2]);
                                                        gameLosses = Int32.Parse(allLines[i + 3]);
                                                        matchWins = Int32.Parse(allLines[i + 4]);
                                                        matchLosses = Int32.Parse(allLines[i + 5]);

                                                        tieBreaker = tieBreaker + 1;

                                                        allLines[i + 1] = tieBreaker.ToString();
                                                        allLines[i + 2] = gameWins.ToString();
                                                        allLines[i + 3] = gameLosses.ToString();
                                                        allLines[i + 4] = matchWins.ToString();
                                                        allLines[i + 5] = matchLosses.ToString();
                                                    }
                                                }
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                    }
                                    for (int i = 0; i < pairingList.Items.Count; i++)
                                    {
                                        numResultsFound = 0;
                                        PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                                        roundInfo.Add("Table: ");
                                        roundInfo.Add(pph.table);
                                        roundInfo.Add(pph.playerOneName);
                                        roundInfo.Add(pph.playerTwoName);
                                        if (pph.g1 != "null" && !string.IsNullOrEmpty(pph.g1))
                                        {
                                            roundInfo.Add("G1: " + pph.g1);
                                        }
                                        else
                                        {
                                            roundInfo.Add("G1: NA");
                                        }
                                        if (pph.g2 != "null" && !string.IsNullOrEmpty(pph.g2))
                                        {
                                            roundInfo.Add("G2: " + pph.g2);
                                        }
                                        else
                                        {
                                            roundInfo.Add("G2: NA");
                                        }
                                        if (pph.g3 != "null" && !string.IsNullOrEmpty(pph.g3))
                                        {
                                            roundInfo.Add("G3: " + pph.g3);
                                        }
                                        else
                                        {
                                            roundInfo.Add("G3: NA");
                                        }
                                        roundInfo.Add("Results: ");
                                        roundInfo.Add(pph.results);

                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Setting results for: " + pph.playerOneName + " vs " + pph.playerTwoName, this.GetType().Name);
                                        }

                                        if (pph.results.Substring(0, 1) == "2")
                                        {
                                            foreach (var file in dir.GetFiles())
                                            {
                                                if (file.Name == tournamentName + ".txt")
                                                {
                                                    string[] allLines = File.ReadAllLines(file.FullName);
                                                    for (int a = 0; a < allLines.Length; a++)
                                                    {
                                                        if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                        {
                                                            if (debugOn)
                                                            {
                                                                debugRef.writeDebugMessage(pph.playerOneName + " won the match, updating results...", this.GetType().Name);
                                                            }

                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            tieBreaker = tieBreaker + 1;
                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                            matchWins++;

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                        {
                                                            if (debugOn)
                                                            {
                                                                debugRef.writeDebugMessage(pph.playerTwoName + " lost the match, updating results...", this.GetType().Name);
                                                            }

                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                            matchLosses++;

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (numResultsFound == 2)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    File.WriteAllLines(file.FullName, allLines);
                                                }
                                            }
                                        }
                                        else if (pph.results.Substring(2, 1) == "2")
                                        {
                                            foreach (var file in dir.GetFiles())
                                            {
                                                if (file.Name == tournamentName + ".txt")
                                                {
                                                    string[] allLines = File.ReadAllLines(file.FullName);
                                                    for (int a = 0; a < allLines.Length; a++)
                                                    {
                                                        if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                        {
                                                            if (debugOn)
                                                            {
                                                                debugRef.writeDebugMessage(pph.playerTwoName + " won the match, updating results...", this.GetType().Name);
                                                            }

                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            tieBreaker = tieBreaker + 1;
                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                            matchWins++;

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                        {
                                                            if (debugOn)
                                                            {
                                                                debugRef.writeDebugMessage(pph.playerOneName + " lost the match, updating results...", this.GetType().Name);
                                                            }

                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                            matchLosses++;

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (numResultsFound == 2)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    File.WriteAllLines(file.FullName, allLines);
                                                }
                                            }
                                        }
                                        else if (pph.results == "1-1")
                                        {
                                            if (debugOn)
                                            {
                                                debugRef.writeDebugMessage("Match was a draw, updating results...", this.GetType().Name);
                                            }

                                            foreach (var file in dir.GetFiles())
                                            {
                                                if (file.Name == tournamentName + ".txt")
                                                {
                                                    string[] allLines = File.ReadAllLines(file.FullName);
                                                    for (int a = 0; a < allLines.Length; a++)
                                                    {
                                                        if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                        {
                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                        {
                                                            tieBreaker = Int32.Parse(allLines[a + 1]);
                                                            gameWins = Int32.Parse(allLines[a + 2]);
                                                            gameLosses = Int32.Parse(allLines[a + 3]);
                                                            matchWins = Int32.Parse(allLines[a + 4]);
                                                            matchLosses = Int32.Parse(allLines[a + 5]);

                                                            gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                            gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));

                                                            allLines[a + 1] = tieBreaker.ToString();
                                                            allLines[a + 2] = gameWins.ToString();
                                                            allLines[a + 3] = gameLosses.ToString();
                                                            allLines[a + 4] = matchWins.ToString();
                                                            allLines[a + 5] = matchLosses.ToString();
                                                            numResultsFound++;
                                                        }
                                                        else if (numResultsFound == 2)
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    File.WriteAllLines(file.FullName, allLines);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Uhhhh this isn't supposed to happen, something went wrong.
                                        }
                                    }
                                    if (byeList.Items.Count > 0)
                                    {
                                        roundInfo.Add("BYE Player(s): ");
                                        roundInfo.Add(byeList.Items.GetItemAt(0).ToString());
                                    }

                                    if (!warned)
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Updating our past pairings list...", this.GetType().Name);
                                        }
                                        for (int i = 0; i < pairingList.Items.Count; i++)
                                        {
                                            PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                                            pastPairings.Add(pph.playerOneName.Substring(0, 6) + pph.playerTwoName.Substring(0, 6));
                                            pastPairings.Add(pph.playerTwoName.Substring(0, 6) + pph.playerOneName.Substring(0, 6));
                                        }
                                    }

                                    if (debugOn)
                                    {
                                        int roundRef = round - 1;
                                        debugRef.writeDebugMessage("Past pairings as of Round " + roundRef + ":", this.GetType().Name);
                                        for (int i = 0; i < pastPairings.Count; i++)
                                        {
                                            debugRef.writeDebugMessage(pastPairings[i].Substring(0, 6) + " vs " + pastPairings[i].Substring(6, 6), this.GetType().Name);
                                        }
                                    }


                                    round--;
                                    filePath = path + "Round " + round + ".txt";
                                    round++;
                                    File.WriteAllLines(filePath, roundInfo);

                                    if (playersToDrop.Count > 0)
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Dropping players...", this.GetType().Name);
                                        }

                                        for (int i = 0; i < playersToDrop.Count; i++)
                                        {
                                            playersDropped.Add(playersToDrop[i]);

                                            if (debugOn)
                                            {
                                                debugRef.writeDebugMessage(playersToDrop[i] + " has been added to dropped list.", this.GetType().Name);
                                            }
                                        }

                                        for (int i = 0; i < players.Count; i++)
                                        {
                                            for (int a = 0; a < playersToDrop.Count; a++)
                                            {
                                                if (players[i].Contains(playersToDrop[a].Substring(0, 6)))
                                                {
                                                    players.RemoveAt(i);

                                                    if (debugOn)
                                                    {
                                                        debugRef.writeDebugMessage(playersToDrop[a] + " was removed from player list.", this.GetType().Name);
                                                    }

                                                    if (i != 0)
                                                    {
                                                        i--;
                                                    }
                                                }

                                                if (players[i].Contains("000000 | BYE"))
                                                {
                                                    players.RemoveAt(i);

                                                    if (debugOn)
                                                    {
                                                        debugRef.writeDebugMessage("BYE has been removed.", this.GetType().Name);
                                                    }

                                                    if (i != 0)
                                                    {
                                                        i--;
                                                    }
                                                }
                                            }
                                        }

                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Clearing our playersToDrop list...", this.GetType().Name);
                                        }

                                        playersToDrop.Clear();
                                    }

                                    if (players.Count() % 2 == 0)
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                                for (int i = 0; i < allLines.Count; i++)
                                                {
                                                    if (allLines[i].Contains("000000 | BYE"))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage("Removing BYE...", this.GetType().Name);
                                                        }

                                                        for (int b = 0; b < 6; b++)
                                                        {
                                                            allLines.RemoveAt(i);
                                                        }
                                                        File.WriteAllLines(file.FullName, allLines);
                                                        allLines = File.ReadAllLines(file.FullName).ToList();
                                                        i = 0;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                                for (int i = 0; i < allLines.Count; i++)
                                                {
                                                    if (allLines[i].Contains("000000 | BYE"))
                                                    {
                                                        for (int b = 0; b < 6; b++)
                                                        {
                                                            allLines.RemoveAt(i);
                                                        }

                                                        File.WriteAllLines(file.FullName, allLines);
                                                        allLines = File.ReadAllLines(file.FullName).ToList();
                                                        i = 0;
                                                    }
                                                }
                                            }
                                        }

                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                if (debugOn)
                                                {
                                                    debugRef.writeDebugMessage("Adding a BYE...", this.GetType().Name);
                                                }
                                                List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                                allLines.Add("000000 | BYE");
                                                allLines.Add("0");
                                                allLines.Add("0");
                                                allLines.Add("0");
                                                allLines.Add("0");
                                                allLines.Add("0");
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                        players.Add("000000 | BYE");
                                    }

                                    clearInformation(pairedPlayers);
                                    playersByTb.Clear();

                                    List<int> playerTiebreakers = new List<int>();

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Getting max tiebreaker...", this.GetType().Name);
                                    }

                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            string[] allLines = File.ReadAllLines(file.FullName);
                                            for (int i = 0; i < allLines.Length; i++)
                                            {
                                                for (int a = 0; a < players.Count; a++)
                                                {
                                                    if (allLines[i].Contains(players[a]))
                                                    {
                                                        playerTiebreakers.Add(Int32.Parse(allLines[i + 1]));
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    bool organizingPlayers = true;
                                    int counter = 0;
                                    int maxTiebreaker = playerTiebreakers.Max();

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Max tiebreaker is: " + maxTiebreaker, this.GetType().Name);
                                    }

                                    for (int i = 0; i < playerTiebreakers.Count; i++)
                                    {
                                        if (playerTiebreakers[i] == maxTiebreaker)
                                        {
                                            counter++;
                                        }
                                    }

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Organizing players by tiebreaker...", this.GetType().Name);
                                    }

                                    while (organizingPlayers)
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                if (playerTiebreakers.Count != 0)
                                                {
                                                    maxTiebreaker = playerTiebreakers.Max();
                                                    string[] allLines = File.ReadAllLines(file.FullName);
                                                    for (int i = 0; i < allLines.Length; i++)
                                                    {
                                                        for (int a = 0; a < players.Count; a++)
                                                        {
                                                            if (allLines[i].Contains(players[a]))
                                                            {
                                                                if (Int32.Parse(allLines[i + 1]) == maxTiebreaker)
                                                                {
                                                                    if (playersDropped.Count > 0)
                                                                    {
                                                                        for (int b = 0; b < playersDropped.Count; b++)
                                                                        {
                                                                            if (players[a].Contains(playersDropped[b]))
                                                                            {

                                                                            }
                                                                            else
                                                                            {
                                                                                playersByTb.Add(players[a]);
                                                                                playerTiebreakers.Remove(maxTiebreaker);
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        playersByTb.Add(players[a]);
                                                                        playerTiebreakers.Remove(maxTiebreaker);
                                                                        break;
                                                                    }
                                                                }
                                                            }

                                                        }

                                                    }
                                                }
                                                else
                                                {
                                                    organizingPlayers = false;
                                                }
                                            }
                                        }
                                    }

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Clearing pastPairings list...", this.GetType().Name);
                                    }

                                    pastPairings.Clear();

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Cutting to top...", this.GetType().Name);
                                    }

                                    if (excludeJudgeOrg == true)
                                    {
                                        foreach (var file in dbDir.GetFiles())
                                        {
                                            if (file.FullName.Contains("PegasusRules"))
                                            {

                                            }
                                            else
                                            {
                                                if (File.ReadLines(file.FullName).Skip(7).Take(1).First().Equals("Yes"))
                                                {
                                                    if (debugOn)
                                                    {
                                                        debugRef.writeDebugMessage("Excluding " + File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First() + " as they are J/O.", this.GetType().Name);
                                                    }

                                                    cleanList(playersByTb, File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First());
                                                }
                                            }
                                        }
                                    }

                                    for (int i = 0; i < playersByTb.Count; i++)
                                    {
                                        if (playersAdded < numPlayers)
                                        {
                                            topCut.Add(compareScores(playersByTb[i], numPlayers));
                                            playersAdded = playersAdded + 1;
                                        }

                                        if(wasPlayerRemoved)
                                        {
                                            i = i - 1;
                                            wasPlayerRemoved = false;
                                        }
                                    }

                                    for (int i = 0; i < topCut.Count; i++)
                                    {
                                        if (i % 2 == 0)
                                        {
                                            pairingList.Items.Add(new PlayerPairingHandler
                                            {
                                                playerTwoName = topCut[i + 1],
                                                playerOneName = topCut[i],
                                                results = "0-0",
                                                table = table.ToString(),
                                                vs = "vs",
                                            });
                                            table++;
                                        }
                                    }

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Top cut is: ", this.GetType().Name);
                                        for (int i = 0; i < topCut.Count; i++)
                                        {
                                            debugRef.writeDebugMessage(topCut[i], this.GetType().Name);
                                        }
                                    }

                                }
                                else
                                {
                                }
                            }
                            else //If there are no players to drop, same logic but w/o dropping.
                            {
                                if (byeList.Items.Count > 0)
                                {
                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Adding results for BYE player: " + byeList.Items[0].ToString(), this.GetType().Name);
                                    }

                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            string[] allLines = File.ReadAllLines(file.FullName);
                                            for (int i = 0; i < allLines.Length; i++)
                                            {
                                                if (allLines[i].Contains(byeList.Items[0].ToString()))
                                                {
                                                    tieBreaker = Int32.Parse(allLines[i + 1]);
                                                    gameWins = Int32.Parse(allLines[i + 2]);
                                                    gameLosses = Int32.Parse(allLines[i + 3]);
                                                    matchWins = Int32.Parse(allLines[i + 4]);
                                                    matchLosses = Int32.Parse(allLines[i + 5]);

                                                    tieBreaker = tieBreaker + 1;

                                                    allLines[i + 1] = tieBreaker.ToString();
                                                    allLines[i + 2] = gameWins.ToString();
                                                    allLines[i + 3] = gameLosses.ToString();
                                                    allLines[i + 4] = matchWins.ToString();
                                                    allLines[i + 5] = matchLosses.ToString();
                                                }
                                            }
                                            File.WriteAllLines(file.FullName, allLines);
                                        }
                                    }
                                }
                                for (int i = 0; i < pairingList.Items.Count; i++)
                                {
                                    numResultsFound = 0;
                                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                                    roundInfo.Add("Table: ");
                                    roundInfo.Add(pph.table);
                                    roundInfo.Add(pph.playerOneName);
                                    roundInfo.Add(pph.playerTwoName);
                                    if (pph.g1 != "null" && !string.IsNullOrEmpty(pph.g1))
                                    {
                                        roundInfo.Add("G1: " + pph.g1);
                                    }
                                    else
                                    {
                                        roundInfo.Add("G1: NA");
                                    }
                                    if (pph.g2 != "null" && !string.IsNullOrEmpty(pph.g2))
                                    {
                                        roundInfo.Add("G2: " + pph.g2);
                                    }
                                    else
                                    {
                                        roundInfo.Add("G2: NA");
                                    }
                                    if (pph.g3 != "null" && !string.IsNullOrEmpty(pph.g3))
                                    {
                                        roundInfo.Add("G3: " + pph.g3);
                                    }
                                    else
                                    {
                                        roundInfo.Add("G3: NA");
                                    }
                                    roundInfo.Add("Results: ");
                                    roundInfo.Add(pph.results);

                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Setting results for: " + pph.playerOneName + " vs " + pph.playerTwoName, this.GetType().Name);
                                    }

                                    if (pph.results.Substring(0, 1) == "2")
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int a = 0; a < allLines.Length; a++)
                                                {
                                                    if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage(pph.playerOneName + " won the match, updating results...", this.GetType().Name);
                                                        }

                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        tieBreaker = tieBreaker + 1;
                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                        matchWins++;

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage(pph.playerTwoName + " lost the match, updating results...", this.GetType().Name);
                                                        }

                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                        matchLosses++;

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (numResultsFound == 2)
                                                    {
                                                        break;
                                                    }
                                                }
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                    }
                                    else if (pph.results.Substring(2, 1) == "2")
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int a = 0; a < allLines.Length; a++)
                                                {
                                                    if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage(pph.playerTwoName + " won the match, updating results...", this.GetType().Name);
                                                        }

                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        tieBreaker = tieBreaker + 1;
                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(2, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(0, 1));
                                                        matchWins++;

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                    {
                                                        if (debugOn)
                                                        {
                                                            debugRef.writeDebugMessage(pph.playerOneName + " lost the match, updating results...", this.GetType().Name);
                                                        }

                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));
                                                        matchLosses++;

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (numResultsFound == 2)
                                                    {
                                                        break;
                                                    }
                                                }
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                    }
                                    else if (pph.results == "1-1")
                                    {
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Match was a draw, updating results...", this.GetType().Name);
                                        }

                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.Name == tournamentName + ".txt")
                                            {
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int a = 0; a < allLines.Length; a++)
                                                {
                                                    if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                                                    {
                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                                                    {
                                                        tieBreaker = Int32.Parse(allLines[a + 1]);
                                                        gameWins = Int32.Parse(allLines[a + 2]);
                                                        gameLosses = Int32.Parse(allLines[a + 3]);
                                                        matchWins = Int32.Parse(allLines[a + 4]);
                                                        matchLosses = Int32.Parse(allLines[a + 5]);

                                                        gameWins = gameWins + Int32.Parse(pph.results.Substring(0, 1));
                                                        gameLosses = gameLosses + Int32.Parse(pph.results.Substring(2, 1));

                                                        allLines[a + 1] = tieBreaker.ToString();
                                                        allLines[a + 2] = gameWins.ToString();
                                                        allLines[a + 3] = gameLosses.ToString();
                                                        allLines[a + 4] = matchWins.ToString();
                                                        allLines[a + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (numResultsFound == 2)
                                                    {
                                                        break;
                                                    }
                                                }
                                                File.WriteAllLines(file.FullName, allLines);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //Uhhhh this isn't supposed to happen, something went wrong.
                                    }
                                }
                                if (byeList.Items.Count > 0)
                                {
                                    roundInfo.Add("BYE Player(s): ");
                                    roundInfo.Add(byeList.Items.GetItemAt(0).ToString());
                                }

                                if (!warned)
                                {
                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Updating our past pairings list...", this.GetType().Name);
                                    }
                                    for (int i = 0; i < pairingList.Items.Count; i++)
                                    {
                                        PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                                        pastPairings.Add(pph.playerOneName.Substring(0, 6) + pph.playerTwoName.Substring(0, 6));
                                        pastPairings.Add(pph.playerTwoName.Substring(0, 6) + pph.playerOneName.Substring(0, 6));
                                    }
                                }

                                if (debugOn)
                                {
                                    int roundRef = round - 1;
                                    debugRef.writeDebugMessage("Past pairings as of Round " + roundRef + ":", this.GetType().Name);
                                    for (int i = 0; i < pastPairings.Count; i++)
                                    {
                                        debugRef.writeDebugMessage(pastPairings[i].Substring(0, 6) + " vs " + pastPairings[i].Substring(6, 6), this.GetType().Name);
                                    }
                                }


                                round--;
                                filePath = path + "Round " + round + ".txt";
                                round++;
                                File.WriteAllLines(filePath, roundInfo);

                                clearInformation(pairedPlayers);

                                List<int> playerTiebreakers = new List<int>();

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Getting max tiebreaker...", this.GetType().Name);
                                }

                                foreach (var file in dir.GetFiles())
                                {
                                    if (file.Name == tournamentName + ".txt")
                                    {
                                        string[] allLines = File.ReadAllLines(file.FullName);
                                        for (int i = 0; i < allLines.Length; i++)
                                        {
                                            for (int a = 0; a < players.Count; a++)
                                            {
                                                if (allLines[i].Contains(players[a]))
                                                {
                                                    playerTiebreakers.Add(Int32.Parse(allLines[i + 1]));
                                                }
                                            }
                                        }
                                    }
                                }

                                bool organizingPlayers = true;
                                int counter = 0;
                                int maxTiebreaker = playerTiebreakers.Max();

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Max tiebreaker is: " + maxTiebreaker, this.GetType().Name);
                                }

                                for (int i = 0; i < playerTiebreakers.Count; i++)
                                {
                                    if (playerTiebreakers[i] == maxTiebreaker)
                                    {
                                        counter++;
                                    }
                                }

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Organizing players by tiebreaker...", this.GetType().Name);
                                }

                                while (organizingPlayers)
                                {
                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            if (playerTiebreakers.Count != 0)
                                            {
                                                maxTiebreaker = playerTiebreakers.Max();
                                                string[] allLines = File.ReadAllLines(file.FullName);
                                                for (int i = 0; i < allLines.Length; i++)
                                                {
                                                    for (int a = 0; a < players.Count; a++)
                                                    {
                                                        if (allLines[i].Contains(players[a]))
                                                        {
                                                            if (Int32.Parse(allLines[i + 1]) == maxTiebreaker)
                                                            {
                                                                if (playersDropped.Count > 0)
                                                                {
                                                                    for (int b = 0; b < playersDropped.Count; b++)
                                                                    {
                                                                        if (players[a].Contains(playersDropped[b]))
                                                                        {

                                                                        }
                                                                        else
                                                                        {
                                                                            playersByTb.Add(players[a]);
                                                                            playerTiebreakers.Remove(maxTiebreaker);
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    playersByTb.Add(players[a]);
                                                                    playerTiebreakers.Remove(maxTiebreaker);
                                                                    break;
                                                                }
                                                            }
                                                        }

                                                    }

                                                }
                                            }
                                            else
                                            {
                                                organizingPlayers = false;
                                            }
                                        }
                                    }
                                }

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Clearing pastPairings list...", this.GetType().Name);
                                }

                                pastPairings.Clear();

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Cutting to top...", this.GetType().Name);
                                }

                                if (excludeJudgeOrg == true)
                                {
                                    foreach (var file in dbDir.GetFiles())
                                    {
                                        if (file.FullName.Contains("PegasusRules"))
                                        {

                                        }
                                        else
                                        {
                                            if (File.ReadLines(file.FullName).Skip(7).Take(1).First().Equals("Yes"))
                                            {
                                                if (debugOn)
                                                {
                                                    debugRef.writeDebugMessage("Excluding " + File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First() + " as they are J/O.", this.GetType().Name);
                                                }

                                                cleanList(playersByTb, File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First());
                                            }
                                        }
                                    }
                                }

                                for (int i = 0; i < playersByTb.Count; i++)
                                {
                                    if (playersAdded < numPlayers)
                                    {
                                        topCut.Add(compareScores(playersByTb[i], numPlayers));
                                        playersAdded = playersAdded + 1;
                                    }

                                    if (wasPlayerRemoved)
                                    {
                                        i = i - 1;
                                        wasPlayerRemoved = false;
                                    }
                                }

                                for (int i = 0; i < topCut.Count; i++)
                                {
                                    if (i % 2 == 0)
                                    {
                                        pairingList.Items.Add(new PlayerPairingHandler
                                        {
                                            playerTwoName = topCut[i + 1],
                                            playerOneName = topCut[i],
                                            results = "0-0",
                                            table = table.ToString(),
                                            vs = "vs",
                                        });
                                        table++;
                                    }
                                }

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Top cut is: ", this.GetType().Name);
                                    for (int i = 0; i < topCut.Count; i++)
                                    {
                                        debugRef.writeDebugMessage(topCut[i], this.GetType().Name);
                                    }
                                }

                            }
                            break;
                        case false:
                            MessageBox.Show("Some matches are still unresolved\n" + "Enter all results before continuing", "Exception");
                            break;
                    }
                }
            }
            else
            {
                if (debugOn)
                {
                    debugRef.writeDebugMessage("Cutting to top, split...", this.GetType().Name);
                }

                numCut = numPlayers;
                cutting = true;
                clearInformation(pairedPlayers);
                playersByTb.Clear();
                List<int> playerTiebreakers = new List<int>();

                if (debugOn)
                {
                    debugRef.writeDebugMessage("Getting max tiebreaker...", this.GetType().Name);
                }

                foreach (var file in dir.GetFiles())
                {
                    if (file.Name == tournamentName + ".txt")
                    {
                        string[] allLines = File.ReadAllLines(file.FullName);
                        for (int i = 0; i < allLines.Length; i++)
                        {
                            for (int a = 0; a < players.Count; a++)
                            {
                                if (allLines[i].Contains(players[a]))
                                {
                                    playerTiebreakers.Add(Int32.Parse(allLines[i + 1]));
                                }
                            }
                        }
                    }
                }

                bool organizingPlayers = true;
                int maxTiebreaker = playerTiebreakers.Max();

                if (debugOn)
                {
                    debugRef.writeDebugMessage("Max tiebreaker is: " + maxTiebreaker, this.GetType().Name);
                }

                if (debugOn)
                {
                    debugRef.writeDebugMessage("Organizing players by tiebreaker...", this.GetType().Name);
                }

                while (organizingPlayers)
                {
                    foreach (var file in dir.GetFiles())
                    {
                        if (file.Name == tournamentName + ".txt")
                        {
                            if (playerTiebreakers.Count != 0)
                            {
                                maxTiebreaker = playerTiebreakers.Max();
                                string[] allLines = File.ReadAllLines(file.FullName);
                                for (int i = 0; i < allLines.Length; i++)
                                {
                                    for (int a = 0; a < players.Count; a++)
                                    {
                                        if (allLines[i].Contains(players[a]))
                                        {
                                            if (Int32.Parse(allLines[i + 1]) == maxTiebreaker)
                                            {
                                                if (playersDropped.Count > 0)
                                                {
                                                    for (int b = 0; b < playersDropped.Count; b++)
                                                    {
                                                        if (players[a].Contains(playersDropped[b]))
                                                        {

                                                        }
                                                        else
                                                        {
                                                            playersByTb.Add(players[a]);
                                                            playerTiebreakers.Remove(maxTiebreaker);
                                                            break;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    playersByTb.Add(players[a]);
                                                    playerTiebreakers.Remove(maxTiebreaker);
                                                    break;
                                                }
                                            }
                                        }

                                    }

                                }
                            }
                            else
                            {
                                organizingPlayers = false;
                            }
                        }
                    }
                }

                if (excludeJudgeOrg == true)
                {
                    foreach (var file in dbDir.GetFiles())
                    {
                        if (file.FullName.Contains("PegasusRules"))
                        {

                        }
                        else
                        {
                            if (File.ReadLines(file.FullName).Skip(7).Take(1).First().Equals("Yes"))
                            {
                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Excluding " + File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First() + " as they are J/O.", this.GetType().Name);
                                }

                                cleanList(playersByTb, File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First());
                            }
                        }
                    }
                }

                for (int i = 0; i < playersByTb.Count; i++)
                {
                    if (playersAdded < numPlayers)
                    {
                        topCut.Add(compareScores(playersByTb[i], numPlayers));
                        playersAdded = playersAdded + 1;
                    }

                    if (wasPlayerRemoved)
                    {
                        i = i - 1;
                        wasPlayerRemoved = false;
                    }
                }

                topCut.Reverse();
                endTournament(topCut);
            }
        }
        private string compareScores(string player, int numPlayers)
        {
            int playersTiebreaker = 0;
            int opponentsTiebreaker = 0;
            int playersGameWins = 0;
            int opponentsGameWins = 0;
            string newPlayer = "";
           
            foreach (var file in dir.GetFiles())
            {
                if (file.Name == tournamentName + ".txt")
                {
                    string[] allLines = File.ReadAllLines(file.FullName);
                    for(int i = 0; i < allLines.Length; i++)
                    {
                        if(allLines[i].Contains(player))
                        {
                            playersTiebreaker = Int32.Parse(allLines[i + 1]);
                            playersGameWins = Int32.Parse(allLines[i + 2]);
                        }
                    }
                    for(int i = 0; i < playersByTb.Count; i++)
                    {
                        for (int a = 0; a < allLines.Length; a++)
                        {
                            if (allLines[a].Contains(playersByTb[i]))
                            {
                                opponentsTiebreaker = Int32.Parse(allLines[a + 1]);
                                opponentsGameWins = Int32.Parse(allLines[a + 2]);

                                if (debugOn)
                                {
                                    debugRef.writeDebugMessage("Comparing tiebreakers between: " + player + " & " + playersByTb[i], this.GetType().Name);
                                    debugRef.writeDebugMessage(player + ": TB " + playersTiebreaker + " GW " + playersGameWins, this.GetType().Name);
                                    debugRef.writeDebugMessage(playersByTb[i] + ": TB " + opponentsTiebreaker + " GW " + opponentsGameWins, this.GetType().Name);
                                }

                                if (opponentsTiebreaker == playersTiebreaker)
                                {
                                    if(opponentsGameWins > playersGameWins)
                                    {
                                        if(topCut.Contains(playersByTb[i]))
                                        {
                                            if (debugOn)
                                            {
                                                debugRef.writeDebugMessage("Top cut already contains " + player + " getting someone else.", this.GetType().Name);
                                            }
                                        }
                                        else
                                        {
                                            if (debugOn)
                                            {
                                                debugRef.writeDebugMessage("Opponent beats current player, swapping player and continuing...", this.GetType().Name);
                                            }

                                            player = playersByTb[i];
                                        }
                                    }
                                }
                                else if (opponentsTiebreaker > playersTiebreaker)
                                {
                                    if (debugOn)
                                    {
                                        debugRef.writeDebugMessage("Opponent has better tiebreakers, swapping player and continuing...", this.GetType().Name);
                                    }

                                    player = playersByTb[i];
                                }
                            }
                        }
                    }
                }
            }
            if (topCut.Contains(player) || !playersByTb.Contains(player) || player.Contains("000000 | BYE"))
            {
                if (debugOn)
                {
                    debugRef.writeDebugMessage("Top cut already contains " + player + ", they are J/O and we are excluding or player chosen was the BYE.", this.GetType().Name);
                }

                for (int i = 0; i < playersByTb.Count; i++)
                {
                    if (playersByTb[i].Contains(player))
                    {
                        if(i + 1 < playersByTb.Count)
                        {
                            if (debugOn)
                            {
                                debugRef.writeDebugMessage("Attempting " + playersByTb[i + 1] + "...", this.GetType().Name);
                            }

                            newPlayer = compareScores(playersByTb[i + 1], numPlayers);
                        }
                        else
                        {
                            if (debugOn)
                            {
                                debugRef.writeDebugMessage("Attempting " + playersByTb[0] + "...", this.GetType().Name);
                            }

                            newPlayer = compareScores(playersByTb[0], numPlayers);
                        }
                    }
                }

                if (debugOn)
                {
                    debugRef.writeDebugMessage("Success, returning " + newPlayer + " to be added to top.", this.GetType().Name);
                }

                return newPlayer;
            }
            else
            {
                if (debugOn)
                {
                    debugRef.writeDebugMessage("Success, returning " + player + " to be added to top.", this.GetType().Name);
                }

                cleanList(playersByTb, player);
                wasPlayerRemoved = true;
                return player;
            }
        }
        public void endTournament(List<string> topPlayers)
        {
            string tcString = "";
            int placement = 1;

            if (debugOn)
            {
                debugRef.writeDebugMessage("Ending tournament...", this.GetType().Name);
            }

            for (int i = topPlayers.Count; i == topPlayers.Count; i--)
            {
                if (i == 0)
                {
                    break;
                }
                else
                {
                    tcString = tcString + placement.ToString() + ". " + topPlayers[i - 1] + "\n";
                    topPlayers.RemoveAt(i - 1);
                    placement++;
                }
            }

            MessageBox.Show("Top " + numCut + " is:\n\n" + tcString + "\nClick OK to end tournament.");

            if (debugOn)
            {
                debugRef.writeDebugMessage("Top is: ", this.GetType().Name);
                debugRef.writeDebugMessage(tcString, this.GetType().Name);
            }

            if (playersDropped.Count > 0)
            {
                for (int i = 0; i < playersDropped.Count; i++)
                {
                    players.Add(playersDropped[i]);
                }
            }

            if (debugOn)
            {
                debugRef.writeDebugMessage("Compiling stats for participating players...", this.GetType().Name);
            }

            foreach (var file in dbDir.GetFiles())
            {
                for (int i = 0; i < players.Count; i++)
                {
                    if (file.Name.Contains(players[i].Substring(0, 6)))
                    {
                        string[] allLines = File.ReadAllLines(file.FullName);
                        foreach (var fileTwo in dir.GetFiles())
                        {
                            if (fileTwo.Name == tournamentName + ".txt")
                            {
                                string[] allLinesTour = File.ReadAllLines(fileTwo.FullName);
                                for (int a = 0; a < allLinesTour.Length; a++)
                                {
                                    if (allLinesTour[a].Contains(allLines[1]))
                                    {
                                        gameWins = Int32.Parse(allLines[19]) + Int32.Parse(allLinesTour[a + 2]);
                                        gameLosses = Int32.Parse(allLines[21]) + Int32.Parse(allLinesTour[a + 3]);
                                        matchWins = Int32.Parse(allLines[15]) + Int32.Parse(allLinesTour[a + 4]);
                                        matchLosses = Int32.Parse(allLines[17]) + Int32.Parse(allLinesTour[a + 5]);
                                        totalGames = gameWins + gameLosses;
                                        totalMatches = matchWins + matchLosses;

                                        if (matchWins == 0)
                                        {
                                            mwp = 0;
                                        }
                                        else if (matchWins == totalMatches)
                                        {
                                            mwp = 100;
                                        }
                                        else
                                        {
                                            mwp = (float)matchWins / totalMatches;
                                            mwp = mwp * 100;
                                        }

                                        if (gameWins == 0)
                                        {
                                            gwp = 0;
                                        }
                                        else if (gameWins == totalGames)
                                        {
                                            gwp = 100;
                                        }
                                        else
                                        {
                                            gwp = (float)gameWins / totalGames;
                                            gwp = gwp * 100;
                                        }

                                        mwpInt = (int)Math.Round(mwp, 0);
                                        gwpInt = (int)Math.Round(gwp, 0);

                                        allLines[19] = gameWins.ToString();
                                        allLines[21] = gameLosses.ToString();
                                        allLines[15] = matchWins.ToString();
                                        allLines[17] = matchLosses.ToString();
                                        allLines[11] = mwpInt.ToString();
                                        allLines[13] = gwpInt.ToString();
                                        if (debugOn)
                                        {
                                            debugRef.writeDebugMessage("Stats updated for: " + allLines[1], this.GetType().Name);
                                        }
                                    }
                                }
                            }
                        }
                        File.WriteAllLines(file.FullName, allLines);
                    }
                }
            }

            if (debugOn)
            {
                debugRef.isTournamentOver = true;
                debugRef.writeDebugMessage("Tournament over, close this to return to main screen...", this.GetType().Name);
            }

            if(debugOn)
            {

            }
            else
            {
                MainWindow mainWin = new MainWindow();
                mainWin.Show();

                Close();
            }
        }

        private void pausePlay_OnClick(object sender, RoutedEventArgs e)
        {
            if(round > 0)
            {
                var buttonPressed = sender as Button;
                if (buttonPressed.Name.Equals("pausePlay_Button"))
                {
                    if (timeEntered > -1)
                    {
                        if (!timer1.IsEnabled)
                        {
                            if (minutes > 0 || seconds > 0)
                            {
                                timer1.Start();
                                buttonPressed.Content = "⏸️";
                            }
                        }
                        else if (timer1.IsEnabled)
                        {
                            timer1.Stop();
                            buttonPressed.Content = "▶️";
                        }
                    }
                }
                else if (buttonPressed.Name.Equals("pausePlay_Button_Pegasus"))
                {
                    if (pegasusFormat == true)
                    {
                        if (!timer2.IsEnabled)
                        {
                            if (pMinutes > 0 || pSeconds > 0)
                            {
                                timer2.Start();
                                buttonPressed.Content = "⏸️";
                            }
                        }
                        else if (timer2.IsEnabled)
                        {
                            timer2.Stop();
                            buttonPressed.Content = "▶️";
                        }
                    }
                }
            }
        }
    }
}
