﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Security.Principal;
using System.Threading;

namespace BotBTSSource
{
    public partial class AdjustPairings : Window
    {
        public static string path = MainWindow.playerFolder + @"\Tournament Files\";
        DirectoryInfo dir = new DirectoryInfo(path);
        public List<string> pairingsChanged = new List<string>();
        public List<string> removedTables = new List<string>();
        public string tournamentName;
        public int tiebreaker;
        public int originalCount;

        public AdjustPairings()
        {
            InitializeComponent();
        }

        private void unpair_OnClick(object sender, RoutedEventArgs e)
        {
            if(pairingList.SelectedItem != null)
            {
                PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.SelectedItem;

                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Unpairing " + pph.playerOneName + " & " + pph.playerTwoName, this.GetType().Name);
                }

                if (pph.table != "0")
                {
                    removedTables.Add(pph.table);
                }

                pairingsChanged.Add(pph.playerOneName.Substring(0, 6) + pph.playerTwoName.Substring(0, 6));
                pairingsChanged.Add(pph.playerTwoName.Substring(0, 6) + pph.playerOneName.Substring(0, 6));

                foreach (var file in dir.GetFiles())
                {
                    if (file.Name == tournamentName + ".txt")
                    {
                        string[] allLines = File.ReadAllLines(file.FullName);
                        for (int a = 0; a < allLines.Length; a++)
                        {
                            if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                            {
                                unpairedPlayerList.Items.Add(new PlayerInfoHandler
                                {
                                    name = pph.playerOneName,
                                    tiebreaker = allLines[a + 1],
                                });
                            }
                            else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                            {
                                unpairedPlayerList.Items.Add(new PlayerInfoHandler
                                {
                                    name = pph.playerTwoName,
                                    tiebreaker = allLines[a + 1],
                                });
                            }
                        }
                        File.WriteAllLines(file.FullName, allLines);
                    }
                }
                pairingList.Items.RemoveAt(pairingList.SelectedIndex);

                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Sucessfully removed pairing.", this.GetType().Name);
                }
            }
            else
            {
                //Ask To select Item
            }
        }

        private void unpairFromNew_OnClick(object sender, RoutedEventArgs e)
        {
            if (newPairingList.SelectedItem != null)
            {
                PlayerPairingHandler pph = (PlayerPairingHandler)newPairingList.SelectedItem;

                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Unpairing new pairing for " + pph.playerOneName + " & " + pph.playerTwoName, this.GetType().Name);
                }

                foreach (var file in dir.GetFiles())
                {
                    if (file.Name == tournamentName + ".txt")
                    {
                        string[] allLines = File.ReadAllLines(file.FullName);
                        for (int a = 0; a < allLines.Length; a++)
                        {
                            if (allLines[a].Contains(pph.playerOneName.Substring(0, 6)))
                            {
                                unpairedPlayerList.Items.Add(new PlayerInfoHandler
                                {
                                    name = pph.playerOneName,
                                    tiebreaker = allLines[a + 1],
                                });
                            }
                            else if (allLines[a].Contains(pph.playerTwoName.Substring(0, 6)))
                            {
                                unpairedPlayerList.Items.Add(new PlayerInfoHandler
                                {
                                    name = pph.playerTwoName,
                                    tiebreaker = allLines[a + 1],
                                });
                            }
                        }
                        File.WriteAllLines(file.FullName, allLines);
                    }
                }
                newPairingList.Items.RemoveAt(newPairingList.SelectedIndex);

                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Successfully removed pairing.", this.GetType().Name);
                }

            }
            else
            {
                //Ask To select Item
            }
        }

        private void pair_OnClick(object sender, RoutedEventArgs e)
        {
            if (unpairedPlayerList.SelectedItem != null)
            {
                bool failedToFindP2Slot = true;
                if (newPairingList.Items.Count > 0)
                {
                    PlayerInfoHandler pih = (PlayerInfoHandler)unpairedPlayerList.SelectedItem;

                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Attempting to pair " + pih.name, this.GetType().Name);
                    }

                    for (int i = 0; i < newPairingList.Items.Count; i++)
                    {
                        PlayerPairingHandler pph = (PlayerPairingHandler)newPairingList.Items[i];
                        if (pph.playerOneName.Length > 0 && string.IsNullOrEmpty(pph.playerTwoName))
                        {
                            failedToFindP2Slot = false;
                            pph.playerTwoName = pih.name;
                            unpairedPlayerList.Items.RemoveAt(unpairedPlayerList.SelectedIndex);
                            newPairingList.Items.Refresh();

                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Successfully created new pairing: " + pph.playerOneName + " & " + pph.playerTwoName, this.GetType().Name);
                            }

                            break;
                        }
                    }
                    if(failedToFindP2Slot == true)
                    {
                        newPairingList.Items.Add(new PlayerPairingHandler
                        {
                            playerOneName = pih.name,
                            vs = "vs",
                        });
                        unpairedPlayerList.Items.RemoveAt(unpairedPlayerList.SelectedIndex);

                        if (((Tournament)this.Owner).debugOn)
                        {
                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Added " + pih.name + " to p1 spot, awaiting p2.", this.GetType().Name);
                        }
                    }
                }
                else
                {
                    PlayerInfoHandler pih = (PlayerInfoHandler)unpairedPlayerList.SelectedItem;

                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Attempting to pair " + pih.name, this.GetType().Name);
                    }

                    newPairingList.Items.Add(new PlayerPairingHandler
                    {
                        playerOneName = pih.name,
                        vs = "vs",
                    });
                    unpairedPlayerList.Items.RemoveAt(unpairedPlayerList.SelectedIndex);

                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Added " + pih.name + " to p1 spot, awaiting p2.", this.GetType().Name);
                    }
                }
            }
            else
            {
                //Ask To select Item
            }
        }

        private void repairAll_OnClick(object sender, RoutedEventArgs e)
        {
            if (newPairingList.Items.Count > 0)
            {
                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Repairing all new pairings...", this.GetType().Name);
                }

                for (int i = 0; i <= newPairingList.Items.Count; i++)
                {
                    i = 0;
                    PlayerPairingHandler pph = (PlayerPairingHandler)newPairingList.Items[i];
                    if(string.IsNullOrEmpty(pph.playerTwoName) || string.IsNullOrEmpty(pph.playerOneName))
                    {

                    }
                    else
                    {
                        if (pph.playerOneName == "000000 | BYE" || pph.playerTwoName == "000000 | BYE")
                        {
                            pairingList.Items.Add(new PlayerPairingHandler
                            {
                                playerTwoName = pph.playerTwoName,
                                playerOneName = pph.playerOneName,
                                table = "0",
                                vs = "vs",
                            });
                            newPairingList.Items.RemoveAt(i);

                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Successfully instantiated pairing between: " + pph.playerOneName + " & " + pph.playerTwoName, this.GetType().Name);
                            }
                        }
                        else
                        {
                            pairingList.Items.Add(new PlayerPairingHandler
                            {
                                playerTwoName = pph.playerTwoName,
                                playerOneName = pph.playerOneName,
                                table = removedTables.Min().ToString(),
                                vs = "vs",
                            });
                            removedTables.Remove(removedTables.Min());
                            newPairingList.Items.RemoveAt(i);

                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Successfully instantiated pairing between: " + pph.playerOneName + " & " + pph.playerTwoName, this.GetType().Name);
                            }
                        }
                        if(pairingsChanged.Count > 0)
                        {
                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Cleaning pairings changed of any new pairings that are duplicates of old ones.", this.GetType().Name);
                            }

                            for (int a = 0; a < pairingsChanged.Count; a++)
                            {
                                if (pairingsChanged[a].Contains(pph.playerOneName.Substring(0, 6) + pph.playerTwoName.Substring(0, 6)) 
                                    || pairingsChanged[a].Contains(pph.playerTwoName.Substring(0, 6) + pph.playerOneName.Substring(0, 6)))
                                {
                                    cleanChangedPairings(pairingsChanged, pairingsChanged[a]);
                                    a--;
                                }
                            }
                        }
                    }
                }

                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("New pairings successfully instantiated.", this.GetType().Name);
                }
            }
            else
            {
                //Ask To select Item
            }
        }

        private void repair_OnClick(object sender, RoutedEventArgs e)
        {
            if (newPairingList.SelectedItem != null)
            {
                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Repairing selected new pairing...", this.GetType().Name);
                }

                PlayerPairingHandler pph = (PlayerPairingHandler)newPairingList.SelectedItem;
                if (string.IsNullOrEmpty(pph.playerTwoName) || string.IsNullOrEmpty(pph.playerOneName))
                {

                }
                else
                {
                    if (pph.playerOneName == "000000 | BYE" || pph.playerTwoName == "000000 | BYE")
                    {
                        pairingList.Items.Add(new PlayerPairingHandler
                        {
                            playerTwoName = pph.playerTwoName,
                            playerOneName = pph.playerOneName,
                            table = "0",
                            vs = "vs",
                        });
                        newPairingList.Items.RemoveAt(newPairingList.SelectedIndex);

                        if (((Tournament)this.Owner).debugOn)
                        {
                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Successfully instantiated pairing between: " + pph.playerOneName + " & " + pph.playerTwoName, this.GetType().Name);
                        }
                    }
                    else
                    {
                        pairingList.Items.Add(new PlayerPairingHandler
                        {
                            playerTwoName = pph.playerTwoName,
                            playerOneName = pph.playerOneName,
                            table = removedTables.Min().ToString(),
                            vs = "vs",
                        });
                        removedTables.Remove(removedTables.Min());
                        newPairingList.Items.RemoveAt(newPairingList.SelectedIndex);

                        if (((Tournament)this.Owner).debugOn)
                        {
                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Successfully instantiated pairing between: " + pph.playerOneName + " & " + pph.playerTwoName, this.GetType().Name);
                        }
                    }
                }
                if (pairingsChanged.Count > 0)
                {
                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Cleaning pairings changed of any new pairings that are duplicates of old ones.", this.GetType().Name);
                    }
                    for (int a = 0; a < pairingsChanged.Count; a++)
                    {
                        if (pairingsChanged[a].Contains(pph.playerOneName.Substring(0, 6) + pph.playerTwoName.Substring(0, 6))
                            || pairingsChanged[a].Contains(pph.playerTwoName.Substring(0, 6) + pph.playerOneName.Substring(0, 6)))
                        {
                            cleanChangedPairings(pairingsChanged, pairingsChanged[a]);
                            a--;
                        }
                    }
                }
            }
            else
            {
                //Ask To select Item
            }
        }

        private void save_OnClick(object sender, RoutedEventArgs e)
        {
            if (pairingList.Items.Count > 0 && pairingList.Items.Count == originalCount)
            {
                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Saving new pairings...", this.GetType().Name);
                }

                MessageBoxResult confirmResult = MessageBox.Show("Pairings will be changed" + "\nAre you sure?", "Confirmation Box", MessageBoxButton.YesNo);
                if (confirmResult == MessageBoxResult.Yes)
                {
                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Clearing old pairings and refreshing listview(s)...", this.GetType().Name);
                    }

                    ((Tournament)this.Owner).pairingList.Items.Clear();
                    ((Tournament)this.Owner).byeList.Items.Clear();
                    for (int i = 0; i < pairingList.Items.Count; i++)
                    {
                        PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                        if (pph.playerOneName == "000000 | BYE" || pph.playerTwoName == "000000 | BYE")
                        {
                            if (pph.playerOneName.Contains("000000 | BYE"))
                            {
                                if (((Tournament)this.Owner).debugOn)
                                {
                                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Adding " + pph.playerTwoName + " to the BYE list.", this.GetType().Name);
                                }

                                ((Tournament)this.Owner).byeList.Items.Add(pph.playerTwoName);
                            }
                            else
                            {
                                if (((Tournament)this.Owner).debugOn)
                                {
                                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Adding " + pph.playerOneName + " to the BYE list.", this.GetType().Name);
                                }

                                ((Tournament)this.Owner).byeList.Items.Add(pph.playerOneName);
                            }
                        }
                        else
                        {
                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Adding pairing: " + pph.playerOneName + " & " + pph.playerTwoName, this.GetType().Name);
                            }

                            ((Tournament)this.Owner).pairingList.Items.Add(new PlayerPairingHandler
                            {
                                playerTwoName = pph.playerTwoName,
                                playerOneName = pph.playerOneName,
                                table = pph.table,
                                vs = "vs",
                                results = "0-0",
                            });
                        }
                    }

                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Running cleanup methods...", this.GetType().Name);
                    }

                    ((Tournament)this.Owner).pairingList.Items.Refresh();

                    ((Tournament)this.Owner).byeList.Items.Refresh();

                    ((Tournament)this.Owner).updateAdjustedPairings(pairingsChanged);

                    this.Close();
                }
                else
                {
                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Action cancelled.", this.GetType().Name);
                    }
                }
            }
            else
            {
                if(pairingList.Items.Count != originalCount)
                {
                    MessageBox.Show("Exception Caught\n" + "You haven't paired all players\n" + "Pair all players then try again", "Exception");
                }
            }
        }
        public void cleanChangedPairings(List<string> list, string element)
        {
            while (list.Contains(element))
            {
                list.Remove(element);
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("Adjust Pairings window has been closed.", this.GetType().Name);
            }
        }
    }
}
