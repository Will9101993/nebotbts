﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotBTSSource
{
    class PlayerPairingHandler
    {
        public string playerOneName { get; set; }
        public string playerTwoName { get; set; }
        public string results { get; set; }
        public string table { get; set; }
        public string vs { get; set; }
        public string g1 { get; set; }
        public string g2 { get; set; }
        public string g3 { get; set; }
        public bool isDraw { get; set; }
        public bool p1IsDrop { get; set; }
        public bool p2IsDrop { get; set; }
    }
}
