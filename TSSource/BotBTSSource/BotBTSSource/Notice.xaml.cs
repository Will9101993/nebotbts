﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BotBTSSource
{

    public partial class Notice : Window
    {
        int cutTo;
        public Notice()
        {
            InitializeComponent();
            cutTo_comboBox.Items.Add("4");
            cutTo_comboBox.Items.Add("8");
            cutTo_comboBox.Items.Add("16");
        }

        private void continue_OnClick(object sender, RoutedEventArgs e)
        {
            ((Tournament)this.Owner).clearPairingHistory();
            this.Close();
        }

        private void cut_OnClick(object sender, RoutedEventArgs e)
        {
            bool tooLittlePlayers = false;
            cutTo = Int32.Parse(cutTo_comboBox.Text);

            if (cutTo > ((Tournament)this.Owner).players.Count)
            {
                tooLittlePlayers = true;
            }

            if (tooLittlePlayers == true)
            {
                MessageBox.Show("You're trying to cut to too many players." + "\nTry a lower value", "Exception");
            }
            else
            {
                if (split_checkBox.IsChecked == true)
                {
                    ((Tournament)this.Owner).split = true;
                }
                else
                {

                }
                ((Tournament)this.Owner).warned = true;
                ((Tournament)this.Owner).cut(cutTo);
                this.Close();
            }
        }
    }
}
