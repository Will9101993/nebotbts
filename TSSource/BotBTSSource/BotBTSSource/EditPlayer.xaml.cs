﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Diagnostics;

namespace BotBTSSource
{
    public partial class EditPlayer : Window
    {

        public EditPlayer()
        {
            InitializeComponent();
            storeComboBox.ItemsSource = NewPlayers.storesParticipating;
        }

        public void loadPlayerData()
        {
            playerStats.Items.Clear();
            foreach (var file in ((PlayerManager)this.Owner).dir.GetFiles())
            {
                if (((PlayerManager)this.Owner).targetId == File.ReadLines(file.FullName).Skip(1).Take(1).First())
                {
                    id_Text.Text = ((PlayerManager)this.Owner).targetId;
                    name_TextBox.Text = File.ReadLines(file.FullName).Skip(3).Take(1).First();
                    deck_TextBox.Text = File.ReadLines(file.FullName).Skip(9).Take(1).First();
                    if (File.ReadLines(file.FullName).Skip(5).Take(1).First() == NewPlayers.storesParticipating[0])
                    {
                        storeComboBox.SelectedIndex = 0;
                    }
                    else
                    {
                        storeComboBox.SelectedIndex = 1;
                    }
                    playerStats.Items.Add(new PlayerInfoHandler
                    {
                        gamesRecord = File.ReadLines(file.FullName).Skip(19).Take(1).First() + " - " + File.ReadLines(file.FullName).Skip(21).Take(1).First(),
                        matchesRecord = File.ReadLines(file.FullName).Skip(15).Take(1).First() + " - " + File.ReadLines(file.FullName).Skip(17).Take(1).First(),
                        gwPercentage = File.ReadLines(file.FullName).Skip(13).Take(1).First() + "%",
                        mwPercentage = File.ReadLines(file.FullName).Skip(11).Take(1).First() + "%",
                    });
                    if (File.ReadLines(file.FullName).Skip(7).Take(1).First() == "Yes")
                    {
                        judgeOrgCheckBox.IsChecked = true;
                    }
                    else
                    {
                        //Do Nothing
                    }
                }
                else
                {
                    //Do Nothing
                }
            }
        }

        private void saveButton_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var file in ((PlayerManager)this.Owner).dir.GetFiles())
            {
                if (id_Text.Text == File.ReadLines(file.FullName).Skip(1).Take(1).First())
                {
                    string[] allLines = File.ReadAllLines(file.FullName);
                    allLines[3] = name_TextBox.Text;
                    allLines[5] = storeComboBox.Text;
                    if (judgeOrgCheckBox.IsChecked.Value == true)
                    {
                        allLines[7] = "Yes";
                    }
                    else
                    {
                        allLines[7] = "No";
                    }
                    allLines[9] = deck_TextBox.Text;
                    File.WriteAllLines(file.FullName, allLines);
                    this.Close();
                }
                else
                {
                    //Do Nothing
                }
            }
        }

        private void OnClose(object sender, EventArgs e)
        {
            ((PlayerManager)this.Owner).validateParentLists(id_Text.Text, name_TextBox.Text);
            ((PlayerManager)this.Owner).editPlayerWinOpen = false;
        }
    }
}
