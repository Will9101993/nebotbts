﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Security.Principal;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Drawing;

namespace BotBTSSource
{
    public partial class PegasusFormat : Window
    {
        public static string path = MainWindow.playerFolder;
        DirectoryInfo dir = new DirectoryInfo(path);
        string filePath;
        bool doesFileExist = false;
        public PegasusFormat()
        {
            InitializeComponent();

            if (Directory.Exists(path))
            {
                foreach (var file in dir.GetFiles())
                {
                    if(file.FullName.Contains("PegasusRules"))
                    {
                        doesFileExist = true;
                    }
                    else
                    {

                    }
                }

                if(doesFileExist == false)
                {
                    filePath = path + "PegasusRules" + ".txt";
                    File.WriteAllText(filePath, "");
                }
                else
                {
                    filePath = path + "PegasusRules" + ".txt";
                }
            }
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(path);

                foreach (var file in dir.GetFiles())
                {
                    if (file.FullName.Contains("PegasusRules"))
                    {
                        doesFileExist = true;
                    }
                    else
                    {

                    }
                }

                if (doesFileExist == false)
                {
                    filePath = path + "PegasusRules" + ".txt";
                    File.WriteAllText(filePath, "");
                }
                else
                {
                    filePath = path + "PegasusRules" + ".txt";
                }
            }
        }

        public void loadRules()
        {
            events_listBox.Items.Clear();
            foreach (var file in dir.GetFiles())
            {
                if (file.FullName.Contains("PegasusRules"))
                {
                    string[] allLines = File.ReadAllLines(file.FullName);
                    for (int i = 0; i < allLines.Length; i++)
                    {
                        if(!string.IsNullOrEmpty(allLines[i]) && !string.IsNullOrWhiteSpace(allLines[i]))
                        {
                            events_listBox.Items.Add(allLines[i]);
                        }
                    }
                }
            }
            UpdateScrollBar(events_listBox);
        }

        private void add_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var file in dir.GetFiles())
            {
                if (file.FullName.Contains("PegasusRules"))
                {
                    File.AppendAllText(filePath, Environment.NewLine + newEvent_textBox.Text);
                    events_listBox.Items.Add(newEvent_textBox.Text);
                    newEvent_textBox.Text = "";
                }
            }
            UpdateScrollBar(events_listBox);
        }

        private void newEvent_textBlock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if(!string.IsNullOrEmpty(newEvent_textBox.Text) && !string.IsNullOrWhiteSpace(newEvent_textBox.Text))
                {
                    foreach (var file in dir.GetFiles())
                    {
                        if (file.FullName.Contains("PegasusRules"))
                        {
                            File.AppendAllText(filePath, Environment.NewLine + newEvent_textBox.Text);
                            events_listBox.Items.Add(newEvent_textBox.Text);
                            newEvent_textBox.Text = "";
                        }
                    }
                }
                else
                {

                }
                UpdateScrollBar(events_listBox);
            }
        }

        private void remove_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var file in dir.GetFiles())
            {
                if (file.FullName.Contains("PegasusRules"))
                {
                    List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                    for (int i = 0; i < allLines.Count; i++)
                    {
                        try
                        {
                            if (allLines[i].Equals(events_listBox.Items.GetItemAt(events_listBox.SelectedIndex)))
                            {
                                allLines.RemoveAt(i);
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Make sure you've selected something to remove.", "Exception");
                        }
                    }
                    File.WriteAllLines(file.FullName, allLines);
                    try
                    {
                        events_listBox.Items.Remove(events_listBox.Items.GetItemAt(events_listBox.SelectedIndex));
                    }
                    catch(Exception)
                    {

                    }
                }
            }
        }

        private void edit_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var doesWinExist = Application.Current.Windows.OfType<InputBox>().FirstOrDefault();
                if(doesWinExist == null)
                {
                    InputBox ipbWin = new InputBox();
                    ipbWin.Owner = this;
                    ipbWin.oldInput = events_listBox.Items.GetItemAt(events_listBox.SelectedIndex).ToString();
                    ipbWin.input_textBox.Text = events_listBox.Items.GetItemAt(events_listBox.SelectedIndex).ToString();
                    ipbWin.Show();
                }
                else
                {
                    doesWinExist.Activate();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Make sure you've selected something to edit.", "Exception");
            }
        }

        public void applyChanges(string oldLine, string newLine)
        {
            foreach (var file in dir.GetFiles())
            {
                if (file.FullName.Contains("PegasusRules"))
                {
                    string[] allLines = File.ReadAllLines(file.FullName);
                    for (int i = 0; i < allLines.Length; i++)
                    {
                        if (allLines[i].Equals(oldLine))
                        {
                            allLines[i] = newLine;
                        }
                    }
                    File.WriteAllLines(file.FullName, allLines);
                }
            }

            //We aren't calling loadRules, even though it's the same function, because we want the scrollbar to remain where the user left it when editing as opposed to adding/loading.
            events_listBox.Items.Clear();
            foreach (var file in dir.GetFiles())
            {
                if (file.FullName.Contains("PegasusRules"))
                {
                    string[] allLines = File.ReadAllLines(file.FullName);
                    for (int i = 0; i < allLines.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(allLines[i]) && !string.IsNullOrWhiteSpace(allLines[i]))
                        {
                            events_listBox.Items.Add(allLines[i]);
                        }
                    }
                }
            }
        }

        private void UpdateScrollBar(ListBox listBox)
        {
            if (listBox != null)
            {
                var border = (Border)VisualTreeHelper.GetChild(listBox, 0);
                var scrollViewer = (ScrollViewer)VisualTreeHelper.GetChild(border, 0);
                scrollViewer.ScrollToBottom();
            }
        }

        private void ok_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
