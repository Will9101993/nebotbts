﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Security.Principal;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Drawing;
using System.Text.RegularExpressions;


namespace BotBTSSource
{
    public partial class TournamentConfirm : Window
    {

        public static readonly string[] tournamentTypes = { "Swiss" /* "Single Elim.", "Double Elim."  Maybe add functionality for other tournament types at some point?*/};
        public static string path = MainWindow.playerFolder;
        DirectoryInfo dir = new DirectoryInfo(path);

        public TournamentConfirm()
        {
            InitializeComponent();
            populateTypeComboBox();
        }

        private void populateTypeComboBox()
        {
            typeComboBox.ItemsSource = tournamentTypes;
            typeComboBox.SelectedIndex = 0;
        }

        private void okButton_OnClick(object sender, RoutedEventArgs e)
        {
            int timerRef;
            int pTimerRef;
            bool pFormCheckPassed = false;
            Tournament tournamentWin = new Tournament();
            tournamentWin.players = ((MainWindow)this.Owner).players;
            tournamentWin.tournamentType = typeComboBox.Text;
            tournamentWin.pegasusFormat = (bool)pFormat_checkBox.IsChecked;
            tournamentWin.excludeJudgeOrg = (bool)exclJudOrg_checkBox.IsChecked;
            if(exclJudOrg_checkBox.IsChecked == true)
            {
                int playerCount = Int32.Parse(playerCount_textBlock.Text);
                if(playerCount > 4)
                {
                    try
                    {
                        if (name_textField.Text.Length > 1 && name_textField.Text.Length > 0 && !char.IsDigit(name_textField.Text[1]) && !string.IsNullOrWhiteSpace(name_textField.Text))
                        {
                            tournamentWin.title_textBlock.Text = name_textField.Text;
                            tournamentWin.tournamentName = Regex.Replace(name_textField.Text, @"/", "");

                            if (pFormat_checkBox.IsChecked == true)
                            {
                                if (pTimer_textBox.Text.Length > 0 && Int32.TryParse(pTimer_textBox.Text, out pTimerRef))
                                {
                                    if (pTimerRef > 0)
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.FullName.Contains("PegasusRules"))
                                            {
                                                List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                                for (int i = 0; i < allLines.Count; i++)
                                                {
                                                    if (!string.IsNullOrEmpty(allLines[i]) && !string.IsNullOrWhiteSpace(allLines[i]))
                                                    {
                                                        tournamentWin.pegasusRules.Add(allLines[i]);
                                                    }
                                                }
                                            }
                                            else
                                            {

                                            }
                                        }
                                        tournamentWin.pTimeEntered = pTimerRef;
                                        pFormCheckPassed = true;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Pegasus Format Timer must have a value greater than 0.", "Exception");
                                    }
                                }
                                else
                                {
                                    if (timer_textField.Text.Length > 0)
                                    {
                                        MessageBox.Show("Pegasus Format Timer needs a valid value.", "Exception");
                                    }
                                }
                            }
                            else
                            {
                                pFormCheckPassed = true;
                            }

                            if (timer_textField.Text.Length > 0 && Int32.TryParse(timer_textField.Text, out timerRef) && pFormCheckPassed == true)
                            {
                                if (timerRef > 0)
                                {
                                    tournamentWin.timeEntered = timerRef;
                                    tournamentWin.initialSetup();
                                    tournamentWin.Show();
                                    ((MainWindow)this.Owner).Close();
                                }
                                else
                                {
                                    tournamentWin.timeEntered = -1;
                                    tournamentWin.initialSetup();
                                    tournamentWin.Show();
                                    ((MainWindow)this.Owner).Close();
                                }
                            }
                            else
                            {
                                if (pFormCheckPassed == true)
                                {
                                    if (timer_textField.Text.Length > 0)
                                    {
                                        MessageBox.Show("Timer field must only contain numbers\n" + "Reset timer and try again", "Exception");
                                    }
                                    else
                                    {
                                        tournamentWin.timeEntered = -1;
                                        tournamentWin.initialSetup();
                                        tournamentWin.Show();
                                        ((MainWindow)this.Owner).Close();
                                    }
                                }
                                else
                                {

                                }
                            }
                        }
                        else
                        {
                            if (char.IsDigit(name_textField.Text[1]))
                            {
                                MessageBox.Show("Name's second character cannot be a number\n" + "I know it's weird, there's a reason, I'm sorry\n" + "Please re-enter a new name and try again", "Exception");
                            }
                            else if (string.IsNullOrWhiteSpace(name_textField.Text))
                            {
                                MessageBox.Show("Name field is empty\n" + "Please enter a name and try again", "Exception");
                            }
                            else
                            {
                                MessageBox.Show("Name field is empty\n" + "Please enter a name and try again", "Exception");
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Name has to be longer than one character\n" + "Please re-enter a new name and try again", "Exception");
                    }
                }
                else
                {
                    MessageBox.Show("To exclude J/O you need more than 4 players\n" + "As that's the minimum to cut to", "Exception");
                }
            }
            else
            {
                try
                {
                    if (name_textField.Text.Length > 1 && name_textField.Text.Length > 0 && !char.IsDigit(name_textField.Text[1]) && !string.IsNullOrWhiteSpace(name_textField.Text))
                    {
                        tournamentWin.title_textBlock.Text = name_textField.Text;
                        tournamentWin.tournamentName = Regex.Replace(name_textField.Text, @"/", "");

                        if (pFormat_checkBox.IsChecked == true)
                        {
                            int lineCounter = 0;
                            foreach (var file in dir.GetFiles())
                            {
                                if (file.FullName.Contains("PegasusRules"))
                                {
                                    List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                    for (int i = 0; i < allLines.Count; i++)
                                    {
                                        if (!string.IsNullOrEmpty(allLines[i]) && !string.IsNullOrWhiteSpace(allLines[i]))
                                        {
                                            lineCounter++;
                                        }
                                    }
                                }
                                else
                                {

                                }
                            }
                            if (lineCounter > 1)
                            {
                                if (pTimer_textBox.Text.Length > 0 && Int32.TryParse(pTimer_textBox.Text, out pTimerRef))
                                {
                                    if (pTimerRef > 0)
                                    {
                                        foreach (var file in dir.GetFiles())
                                        {
                                            if (file.FullName.Contains("PegasusRules"))
                                            {
                                                List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                                for (int i = 0; i < allLines.Count; i++)
                                                {
                                                    if (!string.IsNullOrEmpty(allLines[i]) && !string.IsNullOrWhiteSpace(allLines[i]))
                                                    {
                                                        tournamentWin.pegasusRules.Add(allLines[i]);
                                                    }
                                                }
                                            }
                                            else
                                            {

                                            }
                                        }
                                        tournamentWin.pTimeEntered = pTimerRef;
                                        pFormCheckPassed = true;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Pegasus Format Timer must have a value greater than 0.", "Exception");
                                    }
                                }
                                else
                                {
                                    if (timer_textField.Text.Length > 0)
                                    {
                                        MessageBox.Show("Pegasus Format Timer needs a valid value.", "Exception");
                                    }
                                }
                            }
                            else
                            {
                                MessageBox.Show("Please create at least 2 Pegasus Rules before starting.", "Exception");
                            }
                        }
                        else
                        {
                            pFormCheckPassed = true;
                        }

                        if (timer_textField.Text.Length > 0 && Int32.TryParse(timer_textField.Text, out timerRef) && pFormCheckPassed == true)
                        {
                            if (timerRef > 0)
                            {
                                tournamentWin.timeEntered = timerRef;
                                tournamentWin.initialSetup();
                                tournamentWin.Show();
                                ((MainWindow)this.Owner).Close();
                            }
                            else
                            {
                                tournamentWin.timeEntered = -1;
                                tournamentWin.initialSetup();
                                tournamentWin.Show();
                                ((MainWindow)this.Owner).Close();
                            }
                        }
                        else
                        {
                            if (pFormCheckPassed == true)
                            {
                                if (timer_textField.Text.Length > 0)
                                {
                                    MessageBox.Show("Timer field must only contain numbers\n" + "Reset timer and try again", "Exception");
                                }
                                else
                                {
                                    tournamentWin.timeEntered = -1;
                                    tournamentWin.initialSetup();
                                    tournamentWin.Show();
                                    ((MainWindow)this.Owner).Close();
                                }
                            }
                            else
                            {

                            }
                        }
                    }
                    else
                    {
                        if (char.IsDigit(name_textField.Text[1]))
                        {
                            MessageBox.Show("Name's second character cannot be a number\n" + "I know it's weird, there's a reason, I'm sorry\n" + "Please re-enter a new name and try again", "Exception");
                        }
                        else if (string.IsNullOrWhiteSpace(name_textField.Text))
                        {
                            MessageBox.Show("Name field is empty\n" + "Please enter a name and try again", "Exception");
                        }
                        else
                        {
                            MessageBox.Show("Name field is empty\n" + "Please enter a name and try again", "Exception");
                        }
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Name has to be longer than one character\n" + "Please re-enter a new name and try again", "Exception");
                }
            }
        }

        private void events_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<PegasusFormat>().FirstOrDefault();
            if(doesWinExist == null)
            {
                PegasusFormat pegWin = new PegasusFormat();
                pegWin.Show();
                pegWin.loadRules();
            }
            else
            {
                doesWinExist.Activate();
            }
        }

        private void pFormat_OnClick(object sender, RoutedEventArgs e)
        {
            if (pFormat_checkBox.IsChecked == true)
            {
                pTimer_textBox.Visibility = Visibility.Visible;
                pFormTimer_textBlock.Visibility = Visibility.Visible;
                min_textBlock_Copy.Visibility = Visibility.Visible;
            }
            else
            {
                pTimer_textBox.Visibility = Visibility.Hidden;
                pFormTimer_textBlock.Visibility = Visibility.Hidden;
                min_textBlock_Copy.Visibility = Visibility.Hidden;
            }
        }
    }
}
