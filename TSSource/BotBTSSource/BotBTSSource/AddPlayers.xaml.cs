﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace BotBTSSource
{
    public partial class AddPlayers : Window
    {

        public bool updatingExternal = false;
        public BindingList<string> playerNames = new BindingList<string>();
        public BindingList<string> enrolledPlayers = new BindingList<string>();
        public BindingList<string> excludedPlayers = new BindingList<string>();
        string movingPlayer;
        static string path = MainWindow.playerFolder;
        DirectoryInfo dir = new DirectoryInfo(path);

        public AddPlayers()
        {
            InitializeComponent();

            if (Directory.Exists(path))
            {
                //Do Nothing
            }
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(path);
            }
            playerEnrollList.ItemsSource = enrolledPlayers;
        }

        private void newPlayers_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<NewPlayers>().FirstOrDefault();
            if(doesWinExist == null)
            {
                NewPlayers addNewPlayersWin = new NewPlayers();
                addNewPlayersWin.Owner = this;
                addNewPlayersWin.Show();
                addNewPlayersWin.parentName = "Add Players";
            }
            else
            {
                doesWinExist.Activate();
            }
        }

        private void playerManager_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<PlayerManager>().FirstOrDefault();
            if (doesWinExist == null)
            {
                PlayerManager playerManagerWin = new PlayerManager();
                playerManagerWin.Owner = this;
                playerManagerWin.Show();
                playerManagerWin.parentName = "Add Players";
            }
            else
            {
                doesWinExist.Activate();
            }
        }

        private void addPlayers_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                movingPlayer = playerList.SelectedItem.ToString();
                for (int i = 0; i < playerNames.Count; i++)
                {
                    if (playerNames[i].Equals(movingPlayer))
                    {
                        playerNames.RemoveAt(i);
                    }
                }
                enrolledPlayers.Add(movingPlayer);
            }
            catch (Exception)
            {
                MessageBox.Show("Make sure to select a valid player\n" + "Before clicking add/remove", "Exception");
            }
        }

        private void removePlayers_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                movingPlayer = playerEnrollList.SelectedItem.ToString();
                for (int i = 0; i < enrolledPlayers.Count; i++)
                {
                    if (enrolledPlayers[i].Equals(movingPlayer))
                    {
                        enrolledPlayers.RemoveAt(i);
                    }
                }
                playerNames.Add(movingPlayer);
            }
            catch(Exception)
            {
                MessageBox.Show("Make sure to select a valid player\n" + "Before clicking add/remove", "Exception");
            }
        }

        private void enrollPlayers_OnClick(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < enrolledPlayers.Count; i++)
            {
                ((MainWindow)this.Owner).players.Add(enrolledPlayers[i]);
            }
            ((MainWindow)this.Owner).updateEnrollList();
            this.Close();
        }

        public void loadPlayers()
        {
            playerList.ItemsSource = playerNames;
            if (((MainWindow)this.Owner).players != null)
            {
                validateLists(((MainWindow)this.Owner).players);
                createExcludeList();
            }
            else
            {
                foreach (var file in dir.GetFiles())
                {
                    playerNames.Add(File.ReadLines(file.FullName).Skip(3).Take(1).First());
                }
            }
        }

        public void updatePlayers()
        {
            playerNames.Clear();
            validateLists(enrolledPlayers);
        }

        public void updateMainWinPlayers()
        {
            updatingExternal = true;
            validateLists(((MainWindow)this.Owner).players);
        }

        private void validateLists(BindingList<string> nameList)
        {
            bool matchFound = false;
            foreach (var file in dir.GetFiles())
            {
                for (int i = 0; i < nameList.Count; i++)
                {
                    if (nameList[i].Contains(File.ReadLines(file.FullName).Skip(1).Take(1).First()))
                    {
                        updateNames(i, nameList[i], nameList);
                        matchFound = true;
                        break;
                    }
                    else
                    {

                    }
                }

                for (int a = 0; a < excludedPlayers.Count; a++)
                {
                    if (excludedPlayers[a].Contains(File.ReadLines(file.FullName).Skip(1).Take(1).First()))
                    {
                        updateNames(a, excludedPlayers[a], excludedPlayers);
                        matchFound = true;
                        break;
                    }
                    else
                    {
                        //Do Nothing
                    }
                }

                if (!matchFound && !updatingExternal)
                {
                    if (file.FullName.Contains("PegasusRules"))
                    {

                    }
                    else
                    {
                        try
                        {
                            playerNames.Add(File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First());
                        }
                        catch(Exception)
                        {
                            MessageBox.Show("There's an unallowed file in our directory, please delete it.", "Exception");
                        }
                    }
                }
                else
                {
                    matchFound = false;
                }

            }
            if (updatingExternal == true)
            {
                updatingExternal = false;
            }
        }

        public void cleanLists(string i, string name)
        {
            string properName = i + " | " + name;
            if (playerNames.Contains(properName))
            {
                playerNames.Remove(properName);
            }
            if (enrolledPlayers.Contains(properName))
            {
                enrolledPlayers.Remove(properName);
            }
            if (excludedPlayers.Contains(properName))
            {
                enrolledPlayers.Remove(properName);
            }
            if (((MainWindow)this.Owner).players.Contains(properName))
            {
                enrolledPlayers.Remove(properName);
            }
        }

        public void updateNames(int i, string name, BindingList<string> nameList)
        {
            string id;
            id = name.Substring(0, 6);
            foreach (var file in dir.GetFiles())
            {
                if (File.ReadLines(file.FullName).Skip(1).Take(1).First() == id)
                {
                    nameList[i] = File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First();
                }
            }
        }

        private void createExcludeList()
        {
            for (int i = 0; i < ((MainWindow)this.Owner).players.Count; i++)
            {
                excludedPlayers.Add(((MainWindow)this.Owner).players[i]);
            }
        }

        private void search_textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string userInput = search_textBox.Text;

            if (string.IsNullOrEmpty(userInput))
            {
                playerList.ItemsSource = playerNames;
            }
            else
            {
                if (userInput.ToUpper().Contains("STORE:"))
                {
                    userInput = Regex.Replace(userInput, @"\s", "");
                    string storeName = "";
                    BindingList<string> searchedPlayers = new BindingList<string>();

                    playerList.ItemsSource = searchedPlayers;

                    foreach (var file in dir.GetFiles())
                    {
                        if(file.FullName.Contains("PegasusRules"))
                        {

                        }
                        else
                        {
                            storeName = Regex.Replace(File.ReadLines(file.FullName).Skip(5).Take(1).First(), @"\s", "");
                            if (storeName.ToUpper().Contains(userInput.ToUpper().Substring(6, userInput.Length - 6)))
                            {
                                searchedPlayers.Add(File.ReadLines(file.FullName).Skip(1).Take(1).First() + " | " + File.ReadLines(file.FullName).Skip(3).Take(1).First());
                            }
                        }
                    }
                }
                else
                {
                    BindingList<string> searchedPlayers = new BindingList<string>();
                    string playerName = "";

                    userInput = Regex.Replace(userInput, @"\s", "");
                    playerList.ItemsSource = searchedPlayers;

                    for (int i = 0; i < playerNames.Count; i++)
                    {
                        playerName = Regex.Replace(playerNames[i], @"\s", "");
                        if (playerName.ToUpper().Contains(userInput.ToUpper()))
                        {
                            searchedPlayers.Add(playerNames[i]);
                        }
                    }
                }
            }
        }

        private void clear_OnClick(object sender, RoutedEventArgs e)
        {
            search_textBox.Text = "";

            playerList.ItemsSource = playerNames;
        }
    }
}
