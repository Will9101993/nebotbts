﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotBTSSource
{
    public class PlayerInfoHandler
    {
        public string name { get; set; }
        public string id { get; set; }
        public string deck { get; set; }
        public string store { get; set; }
        public string org { get; set; }
        public string gamesRecord { get; set; }
        public string matchesRecord { get; set; }
        public string gwPercentage { get; set; }
        public string mwPercentage { get; set; }
        public string tiebreaker { get; set; }

    }
}
