﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BotBTSSource
{
    public partial class NewPlayers : Window
    {

        static string path = MainWindow.playerFolder;
        DirectoryInfo dir = new DirectoryInfo(path);
        string filePath;
        private string[] playerInfo = {"ID: ", ""/*ID Field (1)*/, "Name: ", ""/*Name Field (3)*/, "Store: ", ""/*Store Field (5)*/, "Judge/Organizer: ", ""/*Judge Field (7)*/,
            "Main Deck: ", ""/*Main Deck Field (9)*/, "M/W %: ", ""/*M/W % Field (11)*/, "G/W %: ", ""/*G/W % Field (13)*/, "Matches Won: ", ""/*Matches Won Field (15)*/,
            "Matches Lost: ", ""/*Matches Lost Field (17)*/, "Games Won: ", ""/*Games Won Field (19)*/, "Games Lost: ", ""/*Games Lost Field (21)*/};
        private string playerName;
        private string playerDeck;
        public string parentName;
        private int playerId;
        Random randId = new Random();
        public static readonly string[] storesParticipating = {"Game King","Imperial"};

        public NewPlayers()
        {
            InitializeComponent();
            populateStoreComboBox();
        }

        private void populateStoreComboBox()
        {
            storeComboBox.ItemsSource = storesParticipating;
            storeComboBox.SelectedIndex = 0;
        }

        private void confirmButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(path))
            {
                //Do Nothing
            } 
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(path);
            }
            playerName = name_textField.Text;
            playerDeck = mainDeck_textField.Text;
            createId();
            for (int i = 0; i <= playerInfo.Length; i++)
            {
                switch (i)
                {
                    case 1:
                        playerInfo[i] = playerId.ToString();
                        break;
                    case 3:
                        playerInfo[i] = playerName;
                        break;
                    case 5:
                        playerInfo[i] = storeComboBox.Text;
                        break;
                    case 7:
                        if (judgeOrgCheckBox.IsChecked.Value == true)
                        {
                            playerInfo[i] = "Yes";
                        }
                        else
                        {
                            playerInfo[i] = "No";
                        }
                        break;
                    case 9:
                        playerInfo[i] = playerDeck;
                        break;
                    case 11:
                        playerInfo[i] = "0";
                        break;
                    case 13:
                        playerInfo[i] = "0";
                        break;
                    case 15:
                        playerInfo[i] = "0";
                        break;
                    case 17:
                        playerInfo[i] = "0";
                        break;
                    case 19:
                        playerInfo[i] = "0";
                        break;
                    case 21:
                        playerInfo[i] = "0";
                        break;
                }
            }
            filePath = path + playerId + ".txt";
            File.WriteAllLines(filePath, playerInfo);
            switch (parentName)
            {
                case "Add Players":
                    ((AddPlayers)this.Owner).updatePlayers();
                    break;
                case "Player Manager":
                    ((PlayerManager)this.Owner).validateParentLists(name_textField.Text, name_textField.Text);
                    break;
            }
            this.Close();
        }

        private void createId()
        {
            playerId = randId.Next(100000, 999999);
            checkId();
        }

        private void checkId()
        {
            bool matchFound = false;

            foreach (var file in dir.GetFiles())
            {
                if (playerId.ToString() == File.ReadLines(file.FullName).Skip(1).Take(1).First())
                {
                    matchFound = true;
                }
                else
                {
                    //Do Nothing
                }
            }

            if (matchFound == true)
            {
                createId();
            } 
            else
            {
                //Do Nothing
            }
        }
    }
}
