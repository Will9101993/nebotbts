﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Media;
using System.ComponentModel;
using System.Drawing;

namespace BotBTSSource
{

    public partial class Time : Window
    {
        SoundPlayer sound = new SoundPlayer(System.IO.Path.Combine(Environment.CurrentDirectory, @"Audio\", "timeintheround.wav"));
        string extension;
        public Time()
        {
            InitializeComponent();
            sound.PlayLooping();
        }

        private void ok_OnClick(object sender, RoutedEventArgs e)
        {
            extension = extention_textField.Text;

            if(extension.Length > 0 && !string.IsNullOrWhiteSpace(extension))
            {
                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Extension of " + extension + " minutes entered.", this.GetType().Name);
                }

                try
                {
                    if(Int32.Parse(extension) > 0)
                    {
                        ((Tournament)this.Owner).minutes = Int32.Parse(extension);
                        ((Tournament)this.Owner).startTimerExt();
                        sound.Stop();

                        if (((Tournament)this.Owner).debugOn)
                        {
                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Closing Time window...", this.GetType().Name);
                        }

                        Close();
                    }
                    else
                    {
                        sound.Stop();

                        if (((Tournament)this.Owner).debugOn)
                        {
                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Closing Time window...", this.GetType().Name);
                        }

                        Close();
                    }
                }
                catch (Exception)
                {
                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Extension of " + extension + " minutes entered.", this.GetType().Name);
                    }

                    MessageBox.Show("Timer can only include numbers" + "\nTry again", "Exception");
                }
            }
            else
            {
                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Closing Time window...", this.GetType().Name);
                }

                sound.Stop();
                Close();
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("Closing Time window...", this.GetType().Name);
            }

            sound.Stop();
        }
    }
}
