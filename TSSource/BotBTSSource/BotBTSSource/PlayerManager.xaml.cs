﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace BotBTSSource
{
    public partial class PlayerManager : Window
    {

        static string path = MainWindow.playerFolder;
        public DirectoryInfo dir = new DirectoryInfo(path);
        public bool editPlayerWinOpen = false;
        public string targetId;
        public string targetName;
        public string parentName;

        public PlayerManager()
        {
            InitializeComponent();
            loadPlayers();
        }

        public void loadPlayers()
        {
            playerList.Items.Clear();
            foreach (var file in dir.GetFiles())
            {
                if(!file.FullName.Contains("PegasusRules"))
                {
                    playerList.Items.Add(new PlayerInfoHandler
                    {
                        id = File.ReadLines(file.FullName).Skip(1).Take(1).First(),
                        name = File.ReadLines(file.FullName).Skip(3).Take(1).First(),
                        deck = File.ReadLines(file.FullName).Skip(9).Take(1).First(),
                        store = File.ReadLines(file.FullName).Skip(5).Take(1).First(),
                        org = File.ReadLines(file.FullName).Skip(7).Take(1).First()
                    });
                }
                else
                {

                }
            }
        }

        private void editPlayer_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<EditPlayer>().FirstOrDefault();
            if(doesWinExist == null)
            {
                EditPlayer editPlayerWin = new EditPlayer();
                editPlayerWin.Owner = this;
                editPlayerWin.Show();
                try
                {
                    PlayerInfoHandler pih = (PlayerInfoHandler)playerList.SelectedItem;
                    targetId = pih.id;
                    editPlayerWin.loadPlayerData();
                    editPlayerWinOpen = true;
                }
                catch (Exception)
                {
                    editPlayerWin.Close();
                    MessageBox.Show("Make sure to select a valid player\n" + "Before pressing edit\n", "Exception");
                }
            }
            else
            {
                doesWinExist.Activate();
            }
        }
        private void newPlayer_OnClick(object sender, RoutedEventArgs e)
        {
            var doesWinExist = Application.Current.Windows.OfType<NewPlayers>().FirstOrDefault();
            if(doesWinExist == null)
            {
                NewPlayers addNewPlayersWin = new NewPlayers();
                addNewPlayersWin.Owner = this;
                addNewPlayersWin.Show();
                addNewPlayersWin.parentName = "Player Manager";
            }
            else
            {
                doesWinExist.Activate();
            }
        }

        private void delete_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                PlayerInfoHandler pih = (PlayerInfoHandler)playerList.SelectedItem;
                string fileToDelete = dir + pih.id + ".txt";
                targetId = pih.id;
                targetName = pih.name;
                string fileName;
                MessageBoxResult confirmResult = MessageBox.Show("Delete " + targetName + "?", "Confirmation Box", MessageBoxButton.YesNo);
                if(confirmResult == MessageBoxResult.Yes)
                {
                    foreach (var file in dir.GetFiles())
                    {
                        fileName = file.FullName;
                        if (fileName == fileToDelete)
                        {
                            File.Delete(fileToDelete);
                            break;
                        }
                    }
                    loadPlayers();
                    switch (parentName)
                    {
                        case "Add Players":
                            ((AddPlayers)this.Owner).cleanLists(targetId, targetName);
                            break;
                        case "Main Window":
                            ((MainWindow)this.Owner).cleanList(targetId, targetName);
                            break;
                    }
                }
                else
                {
                    //Do Nothing
                }
            }
            catch(Exception)
            {
                MessageBox.Show("Make sure to select a valid player\n" + "Before pressing remove\n", "Exception");
            }
        }

        private void confirmButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void validateParentLists(string id, string name)
        {
            switch (parentName)
            {
                case "Add Players":
                    ((AddPlayers)this.Owner).updatePlayers();
                    ((AddPlayers)this.Owner).updateMainWinPlayers();
                    loadPlayers();
                    break;
                case "Main Window":
                    ((MainWindow)this.Owner).enrollListNameUpdate(id, name);
                    loadPlayers();
                    break;
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (editPlayerWinOpen == true)
            {
                MessageBox.Show("Please finish editing the current player\n" + "Prior to closing the Player Manager", "Exception");
                e.Cancel = true;
            }
            else
            {
                //Do Nothing
            }
        }

        private void search_textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string userInput = search_textBox.Text;

            if (string.IsNullOrEmpty(userInput))
            {
                playerList.Items.Clear();
                foreach (var file in dir.GetFiles())
                {
                    if (file.FullName.Contains("PegasusRules"))
                    {

                    }
                    else
                    {
                        playerList.Items.Add(new PlayerInfoHandler
                        {
                            id = File.ReadLines(file.FullName).Skip(1).Take(1).First(),
                            name = File.ReadLines(file.FullName).Skip(3).Take(1).First(),
                            deck = File.ReadLines(file.FullName).Skip(9).Take(1).First(),
                            store = File.ReadLines(file.FullName).Skip(5).Take(1).First(),
                            org = File.ReadLines(file.FullName).Skip(7).Take(1).First()
                        });
                    }
                }
            }
            else
            {
                if (userInput.ToUpper().Contains("STORE:"))
                {
                    userInput = Regex.Replace(userInput, @"\s", "");
                    string storeName = "";

                    playerList.Items.Clear();
                    foreach (var file in dir.GetFiles())
                    {
                        if (file.FullName.Contains("PegasusRules"))
                        {

                        }
                        else
                        {
                            playerList.Items.Add(new PlayerInfoHandler
                            {
                                id = File.ReadLines(file.FullName).Skip(1).Take(1).First(),
                                name = File.ReadLines(file.FullName).Skip(3).Take(1).First(),
                                deck = File.ReadLines(file.FullName).Skip(9).Take(1).First(),
                                store = File.ReadLines(file.FullName).Skip(5).Take(1).First(),
                                org = File.ReadLines(file.FullName).Skip(7).Take(1).First()
                            });
                        }
                    }

                    for (int i = 0; i < playerList.Items.Count; i++)
                    {
                        PlayerInfoHandler pih = (PlayerInfoHandler)playerList.Items[i];
                        storeName = Regex.Replace(pih.store, @"\s", "");
                        if (!storeName.ToUpper().Contains(userInput.ToUpper().Substring(6, userInput.Length - 6)))
                        {
                            playerList.Items.RemoveAt(i);
                            i = i - 1;
                        }
                    }
                }
                else
                {
                    userInput = Regex.Replace(userInput, @"\s", "");
                    string playerName = "";

                    playerList.Items.Clear();
                    foreach (var file in dir.GetFiles())
                    {
                        if (file.FullName.Contains("PegasusRules"))
                        {

                        }
                        else
                        {
                            playerList.Items.Add(new PlayerInfoHandler
                            {
                                id = File.ReadLines(file.FullName).Skip(1).Take(1).First(),
                                name = File.ReadLines(file.FullName).Skip(3).Take(1).First(),
                                deck = File.ReadLines(file.FullName).Skip(9).Take(1).First(),
                                store = File.ReadLines(file.FullName).Skip(5).Take(1).First(),
                                org = File.ReadLines(file.FullName).Skip(7).Take(1).First()
                            });
                        }
                    }

                    for (int i = 0; i < playerList.Items.Count; i++)
                    {
                        PlayerInfoHandler pih = (PlayerInfoHandler)playerList.Items[i];
                        playerName = Regex.Replace(pih.name, @"\s", "");
                        if (!playerName.ToUpper().Contains(userInput.ToUpper()))
                        {
                            playerList.Items.RemoveAt(i);
                            i = i - 1;
                        }
                    }
                }
            }
        }

        private void clear_OnClick(object sender, RoutedEventArgs e)
        {
            search_textBox.Text = "";

            playerList.Items.Clear();

            foreach (var file in dir.GetFiles())
            {
                if (file.FullName.Contains("PegasusRules"))
                {

                }
                else
                {
                    playerList.Items.Add(new PlayerInfoHandler
                    {
                        id = File.ReadLines(file.FullName).Skip(1).Take(1).First(),
                        name = File.ReadLines(file.FullName).Skip(3).Take(1).First(),
                        deck = File.ReadLines(file.FullName).Skip(9).Take(1).First(),
                        store = File.ReadLines(file.FullName).Skip(5).Take(1).First(),
                        org = File.ReadLines(file.FullName).Skip(7).Take(1).First()
                    });
                }
            }
        }
    }
}
