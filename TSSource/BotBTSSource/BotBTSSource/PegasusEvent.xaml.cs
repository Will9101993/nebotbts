﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BotBTSSource
{
    /// <summary>
    /// Interaction logic for PegasusEvent.xaml
    /// </summary>
    public partial class PegasusEvent : Window
    {
        Random rand = new Random();
        public PegasusEvent()
        {
            InitializeComponent();
        }

        public void initialSetup()
        {
            lEvent_textBlock.Text = ((Tournament)this.Owner).lastEvent;
            nEvent_textBlock.Text = ((Tournament)this.Owner).pegasusRules[rand.Next(0, ((Tournament)this.Owner).pegasusRules.Count())];

            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("Last event set to " + lEvent_textBlock.Text, this.GetType().Name);
            }

            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("New event set to " + nEvent_textBlock.Text, this.GetType().Name);
            }
        }

        private void getNew_OnClick(object sender, RoutedEventArgs e)
        {
            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("Getting new event...", this.GetType().Name);
            }

            nEvent_textBlock.Text = ((Tournament)this.Owner).pegasusRules[rand.Next(0, ((Tournament)this.Owner).pegasusRules.Count())];

            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("New event now set to " + nEvent_textBlock.Text, this.GetType().Name);
            }
        }

        private void ok_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ((Tournament)this.Owner).lastEvent = nEvent_textBlock.Text;
            ((Tournament)this.Owner).usedRules.Add(nEvent_textBlock.Text);

            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage(nEvent_textBlock.Text + " added to usedRules list.", this.GetType().Name);
            }

            ((Tournament)this.Owner).cleanList(((Tournament)this.Owner).pegasusRules, nEvent_textBlock.Text);

            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage(nEvent_textBlock.Text + " cleaned from pegasusRules list.", this.GetType().Name);
            }

            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("usedRules new size is: " + ((Tournament)this.Owner).usedRules.Count(), this.GetType().Name);
            }

            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("pegasusRules new size is: " + ((Tournament)this.Owner).pegasusRules.Count(), this.GetType().Name);
            }

            ((Tournament)this.Owner).pStartTimer();

            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("Event confirmed, restarting timer and closing...", this.GetType().Name);
            }
        }
    }
}
