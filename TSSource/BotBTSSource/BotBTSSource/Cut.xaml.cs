﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BotBTSSource
{
    public partial class Cut : Window
    {
        int cutTo;
        public Cut()
        {
            InitializeComponent();
            cutTo_comboBox.Items.Add("4");
            cutTo_comboBox.Items.Add("8");
            cutTo_comboBox.Items.Add("16");
        }

        private void cut_OnClick(object sender, RoutedEventArgs e)
        {
            bool tooLittlePlayers = false;
            cutTo = Int32.Parse(cutTo_comboBox.Text);

            if (cutTo > ((Tournament)this.Owner).players.Count)
            {
                tooLittlePlayers = true;
            }

            if (tooLittlePlayers == true)
            {
                MessageBox.Show("You're trying to cut to too many players." + "\nTry a lower value", "Exception");
            }
            else
            {
                MessageBoxResult confirmResult = MessageBox.Show("You're about to cut to top " + cutTo_comboBox.Text + ".\nAre you sure?", "Confirmation Box", MessageBoxButton.YesNo);
                if (confirmResult == MessageBoxResult.Yes)
                {
                    cutTo = Int32.Parse(cutTo_comboBox.Text);
                    if (split_checkBox.IsChecked == true)
                    {
                        ((Tournament)this.Owner).split = true;
                    }
                    else
                    {

                    }
                    ((Tournament)this.Owner).cut(cutTo);
                    if (((Tournament)this.Owner).timeEntered > -1)
                    {
                        ((Tournament)this.Owner).startTimer();
                        ((Tournament)this.Owner).pausePlay_Button.Content = "⏸️";
                    }
                    else
                    {
                        //Do Nothing
                    }
                    if (((Tournament)this.Owner).pegasusFormat == true)
                    {
                        ((Tournament)this.Owner).lastEvent = "Nothing yet!";
                        if (((Tournament)this.Owner).usedRules.Count > 0)
                        {
                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Leftovers in usedRules, resetting...", this.GetType().Name);
                            }

                            var rulesToReset = ((Tournament)this.Owner).usedRules.Where(item => item.Length > 0).ToList();
                            ((Tournament)this.Owner).usedRules = ((Tournament)this.Owner).usedRules.Except(rulesToReset).ToList();
                            ((Tournament)this.Owner).pegasusRules.AddRange(rulesToReset);

                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("All usedRules should be returned to pegasusRules.", this.GetType().Name);
                            }

                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("usedRules new count: " + ((Tournament)this.Owner).usedRules.Count, this.GetType().Name);
                            }

                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("pegasusRules new count: " + ((Tournament)this.Owner).pegasusRules.Count, this.GetType().Name);
                            }
                        }
                        ((Tournament)this.Owner).pStartTimer();
                        ((Tournament)this.Owner).pausePlay_Button_Pegasus.Content = "⏸️";
                    }
                    else
                    {
                        //Do Nothing
                    }
                    this.Close();
                }
                else
                {

                }
            }
        }
    }
}
