﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Security.Principal;
using System.Threading;

namespace BotBTSSource
{
    public partial class RoundEdit : Window
    {
        public List<string> rounds = new List<string>();
        public List<string> roundsToRemove = new List<string>();
        public List<string> pairingsToRemove = new List<string>();
        public List<string> playersToRank = new List<string>();
        public static string path = MainWindow.playerFolder + @"\Tournament Files\";
        DirectoryInfo dir = new DirectoryInfo(path);
        int numResultsFound;
        int tiebreaker;
        int gameWins;
        int gameLosses;
        int matchWins;
        int matchLosses;
        public string tournamentName;
        bool checkComplete = false;

        public RoundEdit()
        {
            InitializeComponent();
        }

        private void round_listBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("Getting round information for " + round_listBox.Items.GetItemAt(round_listBox.SelectedIndex) + "...", this.GetType().Name);
            }

            clearInformation();
            foreach (var file in dir.GetFiles())
            {
                if (file.Name.Equals(round_listBox.Items.GetItemAt(round_listBox.SelectedIndex) + ".txt"))
                {
                    string[] allLines = File.ReadAllLines(file.FullName);
                    for (int a = 0; a < allLines.Length; a++)
                    {
                        if (allLines[a].Contains("BYE Player(s)"))
                        {
                            byeList.Items.Add(allLines[a + 1]);
                        }
                        else if (allLines[a].Contains("Table: "))
                        {
                            pairingList.Items.Add(new PlayerPairingHandler
                            {
                                playerTwoName = allLines[a + 3],
                                playerOneName = allLines[a + 2],
                                results = allLines[a + 8],
                                table = allLines[a + 1],
                                vs = "vs",
                                g1 = allLines[a + 4].Substring(4, 2),
                                g2 = allLines[a + 5].Substring(4, 2),
                                g3 = allLines[a + 6].Substring(4, 2),
                            });
                        }
                        checkComplete = true;
                    }
                }
                if (checkComplete)
                {
                    break;
                }
            }
            pairingList.Items.Refresh();

            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("Round information loaded.", this.GetType().Name);
            }
        }

      

        private void goTo_OnClick(object sender, RoutedEventArgs e)
        {
            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("Going to " + round_listBox.Items.GetItemAt(round_listBox.SelectedIndex) + "...", this.GetType().Name);
            }

            string rndsRmv = "";
            roundsToRemove.Clear();
            roundsToRemove.Add(round_listBox.Items.GetItemAt(round_listBox.SelectedIndex).ToString());

            for (int i = 0; i < round_listBox.Items.Count; i++)
            {
                if (i > round_listBox.SelectedIndex)
                {
                    roundsToRemove.Add(round_listBox.Items.GetItemAt(i).ToString());
                }
            }

            for (int i = 0; i < roundsToRemove.Count; i++)
            {
                rndsRmv = rndsRmv + "\n" + roundsToRemove[i];
            }

            MessageBoxResult confirmResult = MessageBox.Show("Results from:\n" + rndsRmv + "\n\nWill be removed" + "\nAre you sure?", "Confirmation Box", MessageBoxButton.YesNo);
            if (confirmResult == MessageBoxResult.Yes)
            {
                for (int i = 0; i < roundsToRemove.Count; i++)
                {
                    foreach (var file in dir.GetFiles())
                    {
                        if (file.Name.Equals(roundsToRemove[i] + ".txt"))
                        {
                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Removing results from " + roundsToRemove[i] + "...", this.GetType().Name);
                            }
                            string[] allLines = File.ReadAllLines(file.FullName);
                            foreach (var fileTwo in dir.GetFiles())
                            {
                                if (fileTwo.Name == tournamentName + ".txt")
                                {
                                    string[] allLinesTwo = File.ReadAllLines(fileTwo.FullName);
                                    for (int a = 0; a < allLines.Length; a++)
                                    {
                                        numResultsFound = 0;
                                        if (allLines[a].Contains("Results:"))
                                        {
                                            if (allLines[a + 1].Substring(0, 1) == "2")
                                            {
                                                for (int b = 0; b < allLinesTwo.Length; b++)
                                                {
                                                    if (allLinesTwo[b].Contains(allLines[a - 5].Substring(0, 6)))
                                                    {
                                                        if (((Tournament)this.Owner).debugOn)
                                                        {
                                                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Removing " + roundsToRemove[i] + " results for " + allLinesTwo[b], this.GetType().Name);
                                                        }

                                                        tiebreaker = Int32.Parse(allLinesTwo[b + 1]) - 1;
                                                        gameWins = Int32.Parse(allLinesTwo[b + 2]) - 2;
                                                        gameLosses = Int32.Parse(allLinesTwo[b + 3]) - Int32.Parse(allLines[a + 1].Substring(2, 1));
                                                        matchWins = Int32.Parse(allLinesTwo[b + 4]) - 1;

                                                        allLinesTwo[b + 1] = tiebreaker.ToString();
                                                        allLinesTwo[b + 2] = gameWins.ToString();
                                                        allLinesTwo[b + 3] = gameLosses.ToString();
                                                        allLinesTwo[b + 4] = matchWins.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (allLinesTwo[b].Contains(allLines[a - 4].Substring(0, 6)))
                                                    {
                                                        if (((Tournament)this.Owner).debugOn)
                                                        {
                                                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Removing " + roundsToRemove[i] + " results for " + allLinesTwo[b], this.GetType().Name);
                                                        }

                                                        gameWins = Int32.Parse(allLinesTwo[b + 2]) - Int32.Parse(allLines[a + 1].Substring(2, 1));
                                                        gameLosses = Int32.Parse(allLinesTwo[b + 3]) - 2;
                                                        matchLosses = Int32.Parse(allLinesTwo[b + 5]) - 1;

                                                        allLinesTwo[b + 2] = gameWins.ToString();
                                                        allLinesTwo[b + 3] = gameLosses.ToString();
                                                        allLinesTwo[b + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (numResultsFound == 2)
                                                    {
                                                        break;
                                                    }
                                                }
                                                File.WriteAllLines(fileTwo.FullName, allLinesTwo);

                                                if (((Tournament)this.Owner).debugOn)
                                                {
                                                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Removed pairing history between " + allLines[a - 5] + " & " + allLines[a - 4], this.GetType().Name);
                                                }

                                                pairingsToRemove.Add(allLines[a - 5].Substring(0, 6) + allLines[a - 4].Substring(0, 6));
                                                pairingsToRemove.Add(allLines[a - 4].Substring(0, 6) + allLines[a - 5].Substring(0, 6));
                                            }
                                            else if (allLines[a + 1].Substring(2, 1) == "2")
                                            {
                                                for (int b = 0; b < allLinesTwo.Length; b++)
                                                {
                                                    if (allLinesTwo[b].Contains(allLines[a - 4].Substring(0, 6)))
                                                    {
                                                        if (((Tournament)this.Owner).debugOn)
                                                        {
                                                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Removing " + roundsToRemove[i] + " results for " + allLinesTwo[b], this.GetType().Name);
                                                        }

                                                        tiebreaker = Int32.Parse(allLinesTwo[b + 1]) - 1;
                                                        gameWins = Int32.Parse(allLinesTwo[b + 2]) - 2;
                                                        gameLosses = Int32.Parse(allLinesTwo[b + 3]) - Int32.Parse(allLines[a + 1].Substring(0, 1));
                                                        matchWins = Int32.Parse(allLinesTwo[b + 4]) - 1;

                                                        allLinesTwo[b + 1] = tiebreaker.ToString();
                                                        allLinesTwo[b + 2] = gameWins.ToString();
                                                        allLinesTwo[b + 3] = gameLosses.ToString();
                                                        allLinesTwo[b + 4] = matchWins.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (allLinesTwo[b].Contains(allLines[a - 5].Substring(0, 6)))
                                                    {
                                                        if (((Tournament)this.Owner).debugOn)
                                                        {
                                                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Removing " + roundsToRemove[i] + " results for " + allLinesTwo[b], this.GetType().Name);
                                                        }

                                                        gameWins = Int32.Parse(allLinesTwo[b + 2]) - Int32.Parse(allLines[a + 1].Substring(0, 1));
                                                        gameLosses = Int32.Parse(allLinesTwo[b + 3]) - 2;
                                                        matchLosses = Int32.Parse(allLinesTwo[b + 5]) - 1;

                                                        allLinesTwo[b + 2] = gameWins.ToString();
                                                        allLinesTwo[b + 3] = gameLosses.ToString();
                                                        allLinesTwo[b + 5] = matchLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (numResultsFound == 2)
                                                    {
                                                        break;
                                                    }
                                                }
                                                File.WriteAllLines(fileTwo.FullName, allLinesTwo);

                                                if (((Tournament)this.Owner).debugOn)
                                                {
                                                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Removed pairing history between " + allLines[a - 5] + " & " + allLines[a - 4], this.GetType().Name);
                                                }

                                                pairingsToRemove.Add(allLines[a - 5].Substring(0, 6) + allLines[a - 4].Substring(0, 6));
                                                pairingsToRemove.Add(allLines[a - 4].Substring(0, 6) + allLines[a - 5].Substring(0, 6));
                                            }
                                            else if (allLines[a + 1] == "1-1")
                                            {
                                                for (int b = 0; b < allLinesTwo.Length; b++)
                                                {
                                                    if (allLinesTwo[b].Contains(allLines[a - 4].Substring(0, 6)))
                                                    {
                                                        if (((Tournament)this.Owner).debugOn)
                                                        {
                                                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Removing " + roundsToRemove[i] + " results for " + allLinesTwo[b], this.GetType().Name);
                                                        }

                                                        gameWins = Int32.Parse(allLinesTwo[b + 2]) - 1;
                                                        gameLosses = Int32.Parse(allLinesTwo[b + 3]) - 1;

                                                        allLinesTwo[b + 2] = gameWins.ToString();
                                                        allLinesTwo[b + 3] = gameLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (allLinesTwo[b].Contains(allLines[a - 5].Substring(0, 6)))
                                                    {
                                                        if (((Tournament)this.Owner).debugOn)
                                                        {
                                                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Removing " + roundsToRemove[i] + " results for " + allLinesTwo[b], this.GetType().Name);
                                                        }

                                                        gameWins = Int32.Parse(allLinesTwo[b + 2]) - 1;
                                                        gameLosses = Int32.Parse(allLinesTwo[b + 3]) - 1;

                                                        allLinesTwo[b + 2] = gameWins.ToString();
                                                        allLinesTwo[b + 3] = gameLosses.ToString();
                                                        numResultsFound++;
                                                    }
                                                    else if (numResultsFound == 2)
                                                    {
                                                        break;
                                                    }
                                                }
                                                File.WriteAllLines(fileTwo.FullName, allLinesTwo);

                                                if (((Tournament)this.Owner).debugOn)
                                                {
                                                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Removed pairing history between " + allLines[a - 5] + " & " + allLines[a - 4], this.GetType().Name);
                                                }

                                                pairingsToRemove.Add(allLines[a - 5].Substring(0, 6) + allLines[a - 4].Substring(0, 6));
                                                pairingsToRemove.Add(allLines[a - 4].Substring(0, 6) + allLines[a - 5].Substring(0, 6));
                                            }
                                            else
                                            {
                                                //We shouldn't be here, what happened?
                                            }
                                        }
                                        else if (allLines[a].Contains("BYE Player(s):"))
                                        {
                                            for (int b = 0; b < allLinesTwo.Length; b++)
                                            {
                                                if (allLinesTwo[b].Contains(allLines[a + 1].Substring(0, 6)))
                                                {
                                                    tiebreaker = Int32.Parse(allLinesTwo[b + 1]) - 1;
                                                    allLinesTwo[b + 1] = tiebreaker.ToString();
                                                    break;
                                                }
                                            }
                                            File.WriteAllLines(fileTwo.FullName, allLinesTwo);

                                            if (((Tournament)this.Owner).debugOn)
                                            {
                                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Removed pairing history between " + allLines[a + 1] + " & " + "BYE", this.GetType().Name);
                                            }

                                            pairingsToRemove.Add(allLines[a + 1].Substring(0, 6) + "000000");
                                            pairingsToRemove.Add("000000" + allLines[a + 1].Substring(0, 6));
                                        }
                                    }
                                }
                            }
                            file.Delete();
                        }
                    }
                }

                for (int i = 0; i < ((Tournament)this.Owner).playersDropped.Count; i++)
                {
                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Checking if dropped players need to be readded...", this.GetType().Name);
                    }

                    for (int a = 0; a < pairingList.Items.Count; a++)
                    {
                        PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[a];
                        if (pph.playerOneName.Contains(((Tournament)this.Owner).playersDropped[i].Substring(0, 6)))
                        {
                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Readding " + pph.playerOneName + " to the tournament.", this.GetType().Name);
                            }

                            ((Tournament)this.Owner).players.Add(((Tournament)this.Owner).playersDropped[i]);
                            if (((Tournament)this.Owner).players.Count() % 2 == 0)
                            {
                                //Do Nothing
                            }
                            else
                            {
                                if (!((Tournament)this.Owner).players.Contains("000000 | BYE"))
                                {
                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                            allLines.Add("000000 | BYE");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            File.WriteAllLines(file.FullName, allLines);
                                        }
                                    }
                                    ((Tournament)this.Owner).players.Add("000000 | BYE");
                                }
                            }
                            ((Tournament)this.Owner).cleanList(((Tournament)this.Owner).playersDropped, ((Tournament)this.Owner).playersDropped[i]);
                            break;
                        }
                        else if (pph.playerTwoName.Contains(((Tournament)this.Owner).playersDropped[i].Substring(0, 6)))
                        {
                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Readding " + pph.playerTwoName + " to the tournament.", this.GetType().Name);
                            }

                            ((Tournament)this.Owner).players.Add(((Tournament)this.Owner).playersDropped[i]);
                            if (((Tournament)this.Owner).players.Count() % 2 == 0)
                            {
                                //Do Nothing
                            }
                            else
                            {
                                if (!((Tournament)this.Owner).players.Contains("000000 | BYE"))
                                {
                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                            allLines.Add("000000 | BYE");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            File.WriteAllLines(file.FullName, allLines);
                                        }
                                    }
                                    ((Tournament)this.Owner).players.Add("000000 | BYE");
                                }
                            }
                            ((Tournament)this.Owner).cleanList(((Tournament)this.Owner).playersDropped, ((Tournament)this.Owner).playersDropped[i]);
                            break;
                        }
                        else if(byeList.Items.GetItemAt(0).ToString().Contains(((Tournament)this.Owner).playersDropped[i].Substring(0, 6)))
                        {
                            if (((Tournament)this.Owner).debugOn)
                            {
                                ((Tournament)this.Owner).debugRef.writeDebugMessage("Readding " + ((Tournament)this.Owner).playersDropped[i] + " to the tournament.", this.GetType().Name);
                            }

                            ((Tournament)this.Owner).players.Add(((Tournament)this.Owner).playersDropped[i]);
                            if (((Tournament)this.Owner).players.Count() % 2 == 0)
                            {
                                //Do Nothing
                            }
                            else
                            {
                                if (!((Tournament)this.Owner).players.Contains("000000 | BYE"))
                                {
                                    foreach (var file in dir.GetFiles())
                                    {
                                        if (file.Name == tournamentName + ".txt")
                                        {
                                            List<string> allLines = File.ReadAllLines(file.FullName).ToList();
                                            allLines.Add("000000 | BYE");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            allLines.Add("0");
                                            File.WriteAllLines(file.FullName, allLines);
                                        }
                                    }
                                    ((Tournament)this.Owner).players.Add("000000 | BYE");
                                }
                            }
                            ((Tournament)this.Owner).cleanList(((Tournament)this.Owner).playersDropped, ((Tournament)this.Owner).playersDropped[i]);
                            break;
                        }
                    }
                }

                if (((Tournament)this.Owner).topCutPlayers.Count > 0)
                {
                    if (((Tournament)this.Owner).debugOn)
                    {
                        ((Tournament)this.Owner).debugRef.writeDebugMessage("Readding top cut players...", this.GetType().Name);
                    }

                    for (int i = 0; i < ((Tournament)this.Owner).topCutPlayers.Count; i = 0)
                    {
                        ((Tournament)this.Owner).topCut.Add(((Tournament)this.Owner).topCutPlayers[i]);

                        if (((Tournament)this.Owner).debugOn)
                        {
                            ((Tournament)this.Owner).debugRef.writeDebugMessage("Readding " + ((Tournament)this.Owner).topCutPlayers[i] + " to the tournament.", this.GetType().Name);
                        }

                        ((Tournament)this.Owner).topCutPlayers.RemoveAt(i);
                    }
                }

                ((Tournament)this.Owner).clearInformation(((Tournament)this.Owner).pairedPlayers);

                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Updating our round pairings...", this.GetType().Name);
                }

                for (int i = 0; i < pairingList.Items.Count; i++)
                {
                    PlayerPairingHandler pph = (PlayerPairingHandler)pairingList.Items[i];
                    ((Tournament)this.Owner).pairingList.Items.Add(new PlayerPairingHandler
                    {
                        playerTwoName = pph.playerTwoName,
                        playerOneName = pph.playerOneName,
                        results = pph.results,
                        table = pph.table,
                        vs = "vs",
                        g1 = pph.g1,
                        g2 = pph.g2,
                        g3 = pph.g3,
                    });
                }

                if(byeList.Items.Count > 0)
                {
                    ((Tournament)this.Owner).byeList.Items.Add(byeList.Items.GetItemAt(0));
                }

                for(int i = 0; i < pairingsToRemove.Count; i++)
                {
                    ((Tournament)this.Owner).cleanList(((Tournament)this.Owner).pastPairings, pairingsToRemove[i]);
                }

                ((Tournament)this.Owner).round = ((Tournament)this.Owner).round - roundsToRemove.Count;

                if (!((Tournament)this.Owner).cutting)
                {
                    ((Tournament)this.Owner).roundNum_textBlock.Text = "Round: " + ((Tournament)this.Owner).round;
                }
                else
                {
                    ((Tournament)this.Owner).roundNum_textBlock.Text = "Round: " + ((Tournament)this.Owner).round + " (Cut)";
                }

                ((Tournament)this.Owner).startTimer();

                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Round edit complete, closing instance...", this.GetType().Name);
                }

                Close();
            }
            else
            {
                if (((Tournament)this.Owner).debugOn)
                {
                    ((Tournament)this.Owner).debugRef.writeDebugMessage("Action cancelled.", this.GetType().Name);
                }
            }
        }

        private void clearInformation()
        {
            checkComplete = false;
            byeList.Items.Clear();
            pairingList.Items.Clear();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (((Tournament)this.Owner).debugOn)
            {
                ((Tournament)this.Owner).debugRef.writeDebugMessage("Round Edit window has been closed.", this.GetType().Name);
            }
        }
    }
}
