﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BotBTSSource
{
    public partial class InputBox : Window
    {
        public string oldInput;
        public InputBox()
        {
            InitializeComponent();
        }

        private void save_OnClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(input_textBox.Text) && !string.IsNullOrWhiteSpace(input_textBox.Text))
            {
                ((PegasusFormat)this.Owner).applyChanges(oldInput, input_textBox.Text);
                this.Close();
            }
            else
            {
                MessageBox.Show("Text field cannot be empty", "Exception");
            }
        }
    }
}
